<%
Function fncShowCartCartBuilderConfig(aArgs)
    Set oCart           = New CartBuilder
    Call oCart.LoadConfig("cart")

    If CBool(SFL_USE_SAVE_FOR_LATER) Then
        Set oSFLCart = New SaveForLaterCart
        Call oSFLCart.ConfigureRegularCartForSFL(oCart)
    End If

    If Not CBool(GLOBAL_SHOW_PRICES_IN_CART) Then
        Set oCart.Columns = New Column_Registry
        BuildNoPriceColumns(oCart.Columns)
        oCart.HeaderCase = "custom"
    End If

    Call oCart.AddDynamicJoin("INNER JOIN orders o ON o.o_key = sc.o_id")


    '*
    ' Filter Cart Extrafields
    Dim sCartExtrafields
    If CBool(GLOBAL_USE_SAVED_CARTS) Then
        sCartExtrafields = oCart.ExtraFields & ", o.record_type as o_record_type"
    Else
        sCartExtrafields = oCart.ExtraFields
    End If
    sCartExtrafields = apply_filters(Array("ShowCartFilterCartExtraFields", Nothing), sCartExtrafields)

    oCart.ExtraFields = sCartExtrafields

    oCart.ShowProductWarehouse = True
    oCart.UseProductGroupings = True    
    oCart.ProductGroupCaption = "" '"Caption?" 
    oCart.DrawProdGroupTotal = True
    oCart.ProductGroupingField = "'<!--'+CAST(pw.pos as varchar(3))+'--><b>'+pw.opt5+'</b> '+pw.opt1+'<br>'+pw.opt2"
    oCart.ExtraFields = oCart.ExtraFields & ", pw.pw_key, pw.id pw_id, pw.opt1 pw_opt1,pw.opt2 pw_opt2,pw.opt3 pw_opt3,pw.opt4 pw_opt4,pw.opt5 pw_opt5, pw.name pw_nm," & _
                        "IsNull(osvd.total,0) s_s_total, os.tax_total s_t_total, os.shipment_total, sha.globaladdress, pw.ds pw_ds, " & _
                        "p.searchfield14 drop_ship_type, pw.city as pw_city, pw.state as pw_state, p.searchfield14, p.searchfield15, p.free_shipping, pa.sku_alias"
    oCart.AddDynamicJoin " INNER JOIN order_detail_shipment_map osm ON osm.od_id = od_key " & _
                         "INNER JOIN order_shipments os ON os.o_id = sc.o_id AND os_key = osm.os_id " & _
                         "LEFT JOIN orders_ship_via_details osvd ON osvd.os_id = os_key " & _
                         "LEFT JOIN shipping_addresses sha ON sha_key = os.sha_id "  &_
                         "LEFT JOIN product_aliases pa on pa.p_id=p.p_key"     
End Function   


Function fncLibCartBuilderActionCartItemName(aArgs)
    Dim oCart: Set oCart = aArgs(1)
    Dim bDrawProdDetailLink: bDrawProdDetailLink = aArgs(2)
    Dim sUrl : sUrl = aArgs(3)
    Dim sNm : sNm = aArgs(4) 

    Dim bFreeShippingEligible : bFreeShippingEligible = False
    Dim bIsDropShipOnly : bIsDropShipOnly = False
     
    If Trim(oCart("searchfield14") & "" ) = "eligible" Then
        bFreeShippingEligible = True
    ElseIf Trim(oCart("searchfield14") & "" ) = "only" Then
        bIsDropShipOnly = True
    End If
    
    'get free ship warehouses & standard warehouse'
    sFreeShipSearchString = "searchexact~pw.id~" & oCart("searchfield15")
    sFreeShipWarehouseData = getWarehouseID(sFreeShipSearchString)
    If sFreeShipWarehouseData <> "" Then
        aFreeShipWarehouseData = split(sFreeShipWarehouseData,"~")
        sFreeShipWarehouseID = aFreeShipWarehouseData(0)
        sFreeShipWarehouseOpt5 = aFreeShipWarehouseData(1)
        sFreeShipWarehouseOpt1 = aFreeShipWarehouseData(2)
        sFreeShipWarehouseOpt2 = aFreeShipWarehouseData(3)
    End If

    sStandardShipWarehouseData = getWarehouseID("searchexact~pw.id~99")
    aStandardShipWarehouseData = split(sStandardShipWarehouseData,"~")
    sStandardShipWarehouseID = aStandardShipWarehouseData(0)

    Dim sCurrentWh : sCurrentWh = uSession.SessionField("pw_id")

    Dim rsShipAddresses 
    Set rsShipAddresses = GetPickupOptions()
    Dim sCurrentStore
    Do Until rsShipAddresses.EOF
        If rsShipAddresses("pw_key") = sCurrentWh Then
            sStores = rsShipAddresses("s_add1") & " | " & rsShipAddresses("s_city") & ", " & rsShipAddresses("s_state") & " " & rsShipAddresses("s_zip")
        End If
        rsShipAddresses.MoveNext
    Loop

    rsShipAddresses.MoveFirst

    If bDrawProdDetailLink Then %>
        <a href="<%=sUrl%>">
            <%=sNm%>
        </a><%
    Else
        Response.Write sNm
    End If 

    If Trim(sGetPageName) = "showcart.asp" Then 
        Dim sCartDisplay %>
        <span class='preferred-shipping'>Shipping Method: <%=oCart("pw_opt3")%> <a style= "display:none;" href="#shipping-selector_<%=oCart("od_key")%>" data-toggle="collapse" data-target="#shipping-selector_<%=oCart("od_key")%>">change</a></span>
        <% If Not CBool(instr(lcase(oCart("pw_opt3")),"free store pickup")) Then 
            sCartDisplay = "display: none;"
        End If %>
        <span class='store-location' style="<%=sCartDisplay%>">
            Your Store: <%=oCart("pw_city")%>, <%=oCart("pw_state")%>
            <a style= "display:none;" href="#store-selector_<%=oCart("od_key")%>" data-toggle="collapse" data-target="#store-selector_<%=oCart("od_key")%>">change</a>
        </span>


    <a data-toggle="collapse"  href="#shipping-selection-container_<%=oCart("od_key")%>">Change Your Shipping Option</a>
    
    <input type="hidden" id="alias_<%=oCart("od_key")%>" value ="<%=oCart("sku_alias")%>">
    <% End If %>
    <input type="hidden" id="p_id_<%=oCart("od_key")%>" value = "<%=oCart("sc_p_id")%>">
    <input type="hidden" id="pw_id_<%=oCart("od_key")%>" value ="<%=oCart("pw_key")%>">
    <input type="hidden" id="sku_<%=oCart("od_key")%>" value ="<%=oCart("p_sku")%>">
    <input type="hidden" id="p_sku_<%=oCart("od_key")%>" value ="<%=oCart("p_sku")%>">
   
    <div  style="margin-top: -109.5px; display: none;">
	   <div class="modal-body">
	      <button type="button" class="close" data-dismiss="modal">X</button>
	      <h4 id="store-selector-modal-header">Select a Store for Item Pickup</h4>
	      <fieldset class="radio-selector"> 
                <% Do Until rsShipAddresses.EOF 
                    sPWKey = rsShipAddresses("pw_key")
                    sAddressString = rsShipAddresses("s_add1") & " | " & rsShipAddresses("s_city") & ", " & rsShipAddresses("s_state") & " " & rsShipAddresses("s_zip") %>
                    <label for="store_options_<%=oCart("od_key")%>_<%=sPWKey%>" class="radio">
                        <input type="radio" id="store_options_<%=oCart("od_key")%>_<%=sPWKey%>" name="shippingselection" value="<%=sPWKey%>"> 
                        <b><%=rsShipAddresses("s_city")%></b> 
                        <br>
                        <small class="store-selector-address"><%=sAddressString%></small>
                    </label>
                    <% rsShipAddresses.MoveNext %>
                <% Loop %>
	      </fieldset>
	      <button type="button" class="btn btn-primary store-selector-btn" data-dismiss="modal" >
	      Select This Store
	      </button>
	   </div>
	</div>

    <div  class="modal modal-small fade" data-backdrop="static" tabindex="-1" aria-hidden="true" style="margin-top: -109.5px; display: none;">
        <div class="modal-body">
            <div id="shipping-selection-container">
              <button type="button" class="close" data-dismiss="modal">X</button>
              <h4 id="store-selector-modal-header">Choose Your Shipping Option</h4>
              <fieldset class="radio-selector controls"> 
                    <% If CBool(bFreeShippingEligible) or CBool(bIsDropShipOnly) Then %>
                        <label for="ss_options_<%=oCart("od_key")%>_<%=sFreeShipWarehouseID%>" class="radio">
                            <input type="radio" id="ss_options_<%=oCart("od_key")%>_<%=sFreeShipWarehouseID%>" name="shippingselection" value="<%=sFreeShipWarehouseID%>"> 
                            <%=sFreeShipWarehouseOpt5%>
                            <p class="shipping-ds"><%=sFreeShipWarehouseOpt1%><strong><%=sFreeShipWarehouseOpt2%></strong></p>
                        </label>
                     <% End If %>
                     <% If not CBool(bIsDropShipOnly) Then %>
            	         <label for="ss_options_<%=oCart("od_key")%>_<%=sStandardShipWarehouseID%>" class="radio">
            		         <input type="radio" id="ss_options_<%=oCart("od_key")%>_<%=sStandardShipWarehouseID%>" name="shippingselection" value="<%=sStandardShipWarehouseID%>"> 
                             <b>Standard Shipping <% If CBool(oCart("free_shipping")) Then %> (Free) <% End If %></b>
            		         <p class="shipping-ds">
                                <% If CBool(oCart("free_shipping")) Then %>
                                    Free shipping from Restaurant Equippers.<BR>
                                <% Else %>
                                    Discounted immediate shipping from Restaurant Equippers.<BR>
                                <% End If %>
                                <strong>Lift Gate and Call Ahead Service included</strong>
                             </p>
                     	</label>
            	         <label for="ss_options_pickup_<%=oCart("od_key")%>" class="radio">
                            <% If sCurrentWh <> "" Then 
                                sPickupText = "Chosen store: " & sStores
                                sChangeBtnText = "Change"
                                sPickupValue = sCurrentWh
                                %><input type="radio" name="shippingselection" id="ss_options_pickup_<%=oCart("od_key")%>" value="<%=sPickupValue%>"><%
                            Else 
                                sPickupText = "Free pick up from nearest store."
                                sChangeBtnText = "Choose My Store"
                                sPickupValue = "storepickup"
                                %><input disabled type="radio" name="shippingselection" id="ss_options_pickup_<%=oCart("od_key")%>" value="<%=sPickupValue%>"><%
                            End If %>
                            <strong>Free Store Pickup</strong>
                            <p class="shipping-ds"><span class="pickup-text"><%=sPickupText%></span> <a id="store-selector-btn" href="#" data-toggle="collapse" data-target="#store-selector_<%=oCart("od_key")%>"><%=sChangeBtnText%></a></p>
                     	</label>
                    <% End If %>
              </fieldset>
              <button type="button" class="btn btn-primary shipping-selector-btn" data-dismiss="modal" >
                Select This Method
              </button>
            </div>
        </div>
    </div>
    <%
End Function

' Dm - 8.2.17
' Displays shipping option below each cart item. 
Function fncLibCartBuilderActionAdditionalCartRow(aArgs)
    Dim oCart: Set oCart = aArgs(3)

    Dim bFreeShippingEligible : bFreeShippingEligible = False
    Dim bIsDropShipOnly : bIsDropShipOnly = False
     
    If Trim(oCart("searchfield14") & "" ) = "eligible" Then
        bFreeShippingEligible = True
    ElseIf Trim(oCart("searchfield14") & "" ) = "only" Then
        bIsDropShipOnly = True
    End If
    
    'get free ship warehouses & standard warehouse'
    sFreeShipSearchString = "searchexact~pw.id~" & oCart("searchfield15")
    sFreeShipWarehouseData = getWarehouseID(sFreeShipSearchString)
    If sFreeShipWarehouseData <> "" Then
        aFreeShipWarehouseData = split(sFreeShipWarehouseData,"~")
        sFreeShipWarehouseID = aFreeShipWarehouseData(0)
        sFreeShipWarehouseOpt5 = aFreeShipWarehouseData(1)
        sFreeShipWarehouseOpt1 = aFreeShipWarehouseData(2)
        sFreeShipWarehouseOpt2 = aFreeShipWarehouseData(3)
        sFreeShipWarehousePwId = aFreeShipWarehouseData(4)
    End If

    sStandardShipWarehouseData = getWarehouseID("searchexact~pw.id~99")
    aStandardShipWarehouseData = split(sStandardShipWarehouseData,"~")
    sStandardShipWarehouseID = aStandardShipWarehouseData(0)

    Dim sCurrentWh : sCurrentWh = uSession.SessionField("pw_id")

    Dim rsShipAddresses 
    Set rsShipAddresses = GetPickupOptions()
    Dim sCurrentStore
    Do Until rsShipAddresses.EOF
        If rsShipAddresses("pw_key") = sCurrentWh Then
            sStores = rsShipAddresses("s_add1") & " | " & rsShipAddresses("s_city") & ", " & rsShipAddresses("s_state") & " " & rsShipAddresses("s_zip")
        End If
        rsShipAddresses.MoveNext
    Loop

    rsShipAddresses.MoveFirst
    sWarehouseChecked = ""
    %>
    <tr class="cart-row-std shipping-option-row">
        <td colspan="5"> 
            <div class="shipping-row" >
                <div id="shipping-selection-container_<%=oCart("od_key")%>" class="collapse shipping-option-container">
                    <div id="shipping-selector_<%=oCart("od_key")%>" class="span6 collapse in shipping_container">
                            <div id="shipping-selection-container">
                              <h4 id="store-selector-modal-header">Choose Your Shipping Option</h4>
                              <fieldset class="radio-selector controls"> 
                                    <%
                                     If CBool(bFreeShippingEligible) or CBool(bIsDropShipOnly) Then 
                                        
                                        If oCart("searchfield15") = sFreeShipWarehousePwId Then 
                                            sWarehouseChecked = " checked='checked' "
                                        End If  
                                        %>
                                        <label for="ss_options_<%=oCart("od_key")%>_<%=sFreeShipWarehouseID%>" class="radio">
                                            <input type="radio" id="ss_options_<%=oCart("od_key")%>" name="shippingselection_<%=oCart("od_key")%>" value="<%=sFreeShipWarehouseID%>" <%=sWarehouseChecked%>> 
                                            <%=sFreeShipWarehouseOpt5%>
                                            <p class="shipping-ds"><%=sFreeShipWarehouseOpt1%><strong><%=sFreeShipWarehouseOpt2%></strong></p>
                                        </label><% 
                                        sWarehouseChecked = ""
                                     End If
                                     If not CBool(bIsDropShipOnly) Then 
                                        If oCart("pw_id") = "99" Then 
                                            sWarehouseChecked = " checked='checked' "
                                        End If 
                                        %>
                                         <label for="ss_options_<%=oCart("od_key")%>_<%=sStandardShipWarehouseID%>" class="radio">
                                             <input type="radio" id="ss_options_<%=oCart("od_key")%>" name="shippingselection_<%=oCart("od_key")%>" value="<%=sStandardShipWarehouseID%>" <%=sWarehouseChecked%>> 
                                             <b>Standard Shipping <% If CBool(oCart("free_shipping")) Then %> (Free) <% End If %></b>
                                             <p class="shipping-ds">
                                                <% If CBool(oCart("free_shipping")) Then %>
                                                    Free shipping from Restaurant Equippers.<BR>
                                                <% Else %>
                                                    Discounted immediate shipping from Restaurant Equippers.<BR>
                                                <% End If %>
                                                <strong>Lift Gate and Call Ahead Service included</strong>
                                             </p>
                                        </label><% 
                                        sWarehouseChecked = ""
                                        If CBool(instr(lcase(oCart("pw_opt3")),"free store pickup")) Then 
                                            sWarehouseChecked = " checked='checked' "
                                        End If 
                                        %>
                                         <label for="ss_options_pickup_<%=oCart("od_key")%>" class="radio ss_options_pickup_<%=oCart("od_key")%>">
                                            <% If sCurrentWh <> "" Then 
                                                sPickupText = "Chosen store: " & sStores
                                                sChangeBtnText = "Change"
                                                sPickupValue = sCurrentWh
                                                %>
                                                <input  type="radio"  name="shippingselection_<%=oCart("od_key")%>" id="ss_options_pickup_<%=oCart("od_key")%>" value="<%=sPickupValue%>" <%=sWarehouseChecked%> class="store-pickup"><%
                                            Else 
                                                sPickupText = "Free pick up from nearest store."
                                                sChangeBtnText = "Choose My Store"
                                                sPickupValue = "storepickup"
                                                %><input  type="radio"  name="shippingselection_<%=oCart("od_key")%>" id="ss_options_pickup_<%=oCart("od_key")%>" value="<%=sPickupValue%>" class="store-pickup"><%
                                            End If %>
                                            <strong>Free Store Pickup</strong>
                                            <p class="shipping-ds"><span class="pickup-text"><%=sPickupText%></span> <a style="display:none;" id="store-selector-btn" href="#store-selector_<%=oCart("od_key")%>" data-toggle="collapse" data-target="#store-selector_<%=oCart("od_key")%>"><%=sChangeBtnText%></a></p>
                                        </label>
                                    <% End If %>
                              </fieldset>
                              <button type="button" class="btn btn-primary shipping-selector-btn" value="<%=oCart("od_key")%>" >
                                Select This Method
                              </button>
                        </div>
                    </div><% 
                    If CBool(instr(lcase(oCart("pw_opt3")),"free store pickup")) Then 
                        sStoreSelectorClass= " in "
                    End If 
                    %> 

                    <div id="store-selector_<%=oCart("od_key")%>" class="span6 collapse store_selector_container <%=sStoreSelectorClass%>">
                          <h4 id="store-selector-modal-header">Select a Store for Item Pickup</h4>
                          <fieldset class="radio-selector"> 
                                <% Do Until rsShipAddresses.EOF 
                                    sPWKey = rsShipAddresses("pw_key")
                                    sAddressString = rsShipAddresses("s_add1") & " | " & rsShipAddresses("s_city") & ", " & rsShipAddresses("s_state") & " " & rsShipAddresses("s_zip")

                                    sStoreSelected = ""
                                    If Lcase(oCart("pw_city")) = Lcase(rsShipAddresses("s_city")) Or (sPickupValue = sPWKey) Then 
                                        sStoreSelected = " checked='checked' "
                                    End If  
                                     %>
                                    
                                    <label for="store_options_<%=oCart("od_key")%>_<%=sPWKey%>" class="radio">
                                        <input type="radio" id="store_options_<%=oCart("od_key")%>_<%=sPWKey%>" name="store_option_<%=oCart("od_key")%>" value="<%=sPWKey%>" <%=sStoreSelected%>> 
                                        <b><%=rsShipAddresses("s_city")%></b> 
                                        <br>
                                        <small class="store-selector-address"><%=sAddressString%></small>
                                    </label>
                                    <% rsShipAddresses.MoveNext %>
                                <% Loop %>
                          </fieldset>
                          <button type="button" class="btn btn-primary store-selector-btn" value="<%=oCart("od_key")%>" >
                          Select This Store
                          </button>
                    </div>
                </div>
            </div>
        </td>
    </tr>
    <%

End Function 

Function fncLibCartBuilderFilterDescText(ByRef aArgs, ByVal sVal)
    Dim oCartRecordSet: Set oCartRecordSet = aArgs(1)
    sVal = "<span class=cart-header-label>" & oCartRecordSet("pw_opt5") & "</span><span class=lead-time-text>" & oCartRecordSet("pw_opt1") & "</span><span class=shipment-info>" & oCartRecordSet("pw_opt2") & "</span>"
    fncLibCartBuilderFilterDescText = sVal
End Function

Private Function getWarehouseID(sSearchString)
	Set oRs                     = New cDataAccessRecordSet3
	oRs.SQL_ID                        = "Base_Product_Warehouses"
	oRs.SQL_ID_Minor_VersionNumber    = 0
	oRs.SQL_ID_Revision_VersionNumber = 1
    oRs.ExtraFields                     = "pw.opt5,pw.opt1,pw.opt2, pw.id"
	oRs.SearchString                  = sSearchString
	Call oRs.Open()
	
	If Not oRs.EOF Then
		getWarehouseID = oRs("pw_key") & "~" & oRs("opt5") & "~" & oRs("opt1") & "~" & oRs("opt2") & "~" & oRs("id")
	End If 'If Not oRs.EOF Then
	
	Set oRs = Nothing
End Function

Private Function GetPickupOptions() 
    Set rsShipAddresses                           = New cDataAccessRecordSet3
	rsShipAddresses.SQL_ID                        = "All_Customer_Shipping_Addresses"
	rsShipAddresses.SQL_ID_Minor_VersionNumber    = 0
	rsShipAddresses.SQL_ID_Revision_VersionNumber = 5

	Set rsExemptions = new cDataAccessRecordSet3
	rsExemptions.SQL_ID = "EQUIPPERS_Tax_Exemptions_By_Account"
	rsExemptions.SQL_ID_Minor_VersionNumber = 0
	rsExemptions.SQL_ID_Revision_VersionNumber = 0
	rsExemptions.searchString = "searchexact~a.a_key~" & uSession.AccountKey & "|AND|searchexact~map.exempt~1"
	rsExemptions.Open
	If Not rsExemptions.EOF Then
		sExemptString = ""
		Do While Not rsExemptions.EOF 
			If CBool(rsExemptions("exempt")) then
				If sExemptString <> "" Then
					sExemptString = sExemptString & "~"
				End if
				sExemptString = sExemptString & Trim(rsExemptions("state"))
			End if
			rsExemptions.MoveNext
		Loop

		'Now setup full searchstring...
		sExemptString = "|AND|((searchlist~s_state~" & sExemptString & "|AND|searchexact~tax_exempt~1)|or|((searchlistnot~s_state~" & sExemptString & "|AND|searchexact~tax_exempt~0))"					

	Else
		'- If not records, then only show globals that are NOT tax exempt
		sExemptString = "|AND|searchexact~tax_exempt~0"

	End If				

	rsExemptions.Close
	Set rsExemptions = Nothing

	'* Added condition for ERP Company
	sModalSearchString ="(searchexact~sha.globaladdress~1|and|searchexact~sha.status~1)" & sExemptString '"
	If CBool(GLOBAL_USE_ERP_FILTER_ON_WAREHOUSE) Then
		If sCustomerERPCompany <> "" Then
			sModalSearchString = sModalSearchString & "|and|searchexact~sha.erp_company~" & sCustomerERPCompany 
		End If
	End If 

	rsShipAddresses.SearchString                  = sModalSearchString
	rsShipAddresses.ExtraFields                   = "pw.pw_key, sha.s_city, sha.s_zip, sha.s_add1, sha.s_state"
	rsShipAddresses.AddDynamicJoin 				  "INNER JOIN product_warehouses pw on pw.zip=sha.s_zip"
	Call rsShipAddresses.Open()
	
	If Not rsShipAddresses.EOF Then
		iRecordCount = rsShipAddresses.RecordCount
	End If 'If Not rsShipAddresses.EOF Then


    Set GetPickupOptions = rsShipAddresses
End Function
%>