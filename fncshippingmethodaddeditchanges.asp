<%

Public Function fncAddProdWarehouseSection(aArgs) 
	Set oF = aArgs(1)
	oF.SectionHeader "Filter By Product Warehouse"
	Set oQuestion                    = New Question_Radio
	oQuestion.Props.Name             = "pw_id_filter_type"
	oQuestion.Props.Question         = "Product Warehouse Filter Type:"
	oQuestion.Props.QuestionTemplate = "div"
	oQuestion.Props.Answers          = "All~all|Include On Matching Warehouses~include|Exclude on Matching Warehouses~exclude"
	oQuestion.Props.UseHelp          = True
	oQuestion.Props.HelpTitle        = "Filter by Product Warehouse"
	oQuestion.Props.HelpText         = "Include means that shipments with the selected Warehouse will get this shipping method. If there is no Warehouse set on the shipment then it will not get this method. Exclude means that shipments with a blank or different Warehouse than what is selected below will get this shipping method."
		Set oFormDisplayActivator           = New FormDisplayCondition
		oFormDisplayActivator.Value         = "all"
		oFormDisplayActivator.HideID         = "pw_vir_container"
	oQuestion.Props.AddDisplayActivator = oFormDisplayActivator
	Call oF.AddQuestion(oQuestion)


	Set oQuestion									= New Question_MultiInput
	oQuestion.Props.Name							= "pw_vir"
	oQuestion.Props.Question						= ""
	oQuestion.Props.Label							= "Product Warehouses"
	oQuestion.Props.EntryDelimeter					= ";"
	oQuestion.Props.AnswerString					= "<=id=>"

	'Setup MultiInput to Prepop from Virtual Field
	oQuestion.Props.ListDataSource					= "answers_from_qb"
	oQuestion.Props.SQL_ID							= "Ship_Vias_Product_Warehouses_Mapped"
	oQuestion.Props.SQL_ID_Minor_VersionNumber		= 0
	oQuestion.Props.SQL_ID_Revision_VersionNumber	= 3
	oQuestion.Props.DisplayField					= "pw.name"
	oQuestion.Props.ValueField						= "pw.pw_key"
	oQuestion.Props.SearchString					= "searchexact~pwsvm.sv_id~" & oF.Key

		Set oMIQuestion								= New Question_Text
		oMIQuestion.Props.Name						= "pw_key" '- holds answer string value
		oMIQuestion.Props.Question					= ""
		oMIQuestion.Props.QuestionType				= "hidden"
		oMIQuestion.Props.UseFormDefaults			= False
		oQuestion.Props.AddQuestion					= oMIQuestion

		'MultiInput Questions
		Set oMIQuestion						= New Question_Text
		oMIQuestion.Props.Name				= "id"
		oMIQuestion.Props.Question			= "Product Warehouse:"
		oMIQuestion.Props.QuestionTemplate	= "div"
		oMIQuestion.Props.Size				= 41
		oMIQuestion.Props.UseFindLightbox	= True
		oMIQuestion.Props.DisplayField		= "id"
		oMIQuestion.Props.ValueField		= "pw_key"

			'Lightbox SQL Setup
			oMIQuestion.Props.LightBoxSQL_ID						= "Product_Warehouses"
			oMIQuestion.Props.LightBoxSQL_ID_Minor_VersionNumber	= 0
			oMIQuestion.Props.LightBoxSQL_ID_Revision_VersionNumber = 2
			oMIQuestion.Props.LightBoxExtraFields					= "id, name"
			oMIQuestion.Props.LightBoxOrderBy						= "name~"

			'Lightbox Display Setup
			oMIQuestion.Props.LightBoxTitle							= "Find Product Warehouse"
			oMIQuestion.Props.LightBoxDescription					= "Please select a product warehouse."
			oMIQuestion.Props.LightBoxTableColumns					= "id;ID~name;Product Warehouse"
			oMIQuestion.Props.LightBoxPrimaryKey					= "pw_key"
			oMIQuestion.Props.LightBoxBlankOnLoad					= False

		oQuestion.Props.AddQuestion = oMIQuestion

	Call oF.AddQuestion(oQuestion)

End Function

Function fncPostToProdWarehouseShipViaMap(aArgs)
	Set oPS_ShipVias			= aArgs(1)
	oPS_ShipVias.ReplaceVirtualField( "pw_vir" )
End Function
%>