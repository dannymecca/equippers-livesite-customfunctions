<%

'rmaas 2/1/2017 - catch-all for ATC's without selected pw_id
Public Function PostDefaultProdWarehouse(aArgs)
	Dim plCart, plData, sKey, sProdWarehouse
	Set plCart = aArgs(1)
	Set plData = aArgs(2)
	sKey = aArgs(4)

	If IsPostEdit(plCart) Then
		Exit Function
	End If

	sProdWarehouse = GetStandardShippingWarehouse()

	If Trim(plCart.getPostedValue("pw_id_" & sKey) & "") = "" Then
		plData.ColumnValue("pw_id") = sProdWarehouse
	End If
End Function

Private Function IsPostEdit(plCart)
	IsPostEdit = Trim(plCart.GetPostedValue("unqs") & "") <> ""
End Function

Private Function GetStandardShippingWarehouse()
	Dim sWarehouseKey
	Set rsWarehouses                           = New cDataAccessRecordSet3
	rsWarehouses.SQL_ID                        = "Product_Warehouses"
	rsWarehouses.SQL_ID_Minor_VersionNumber    = 0
	rsWarehouses.SQL_ID_Revision_VersionNumber = 2
	rsWarehouses.SearchString                  = "searchexact~pw.id~99"
	rsWarehouses.MaxRecords                    = 1
	Call rsWarehouses.Open()

	If Not rsWarehouses.EOF Then
		sWarehouseKey = rsWarehouses("pw_key")
	End If 'If Not rsWarehouses.EOF Then
	
	Set rsWarehouses = Nothing

	GetStandardShippingWarehouse = sWarehouseKey
End Function 

'dcarton 5/18/2017 #119718
'This is copied from the cart post logic ElseIf IsPostBack() Then block
'Some process is stripped out, such as bEdit, how iQty is handled.
Private Function AlterPostBackData(aArgs)

	Dim sProdWarehouse
	sQType = LCase(Trim(Request.QueryString("type")&""))

	If sQType <> "reorder" And sQType <> "qsaddprod" and sQType <> "relatedaddcart" Then
		'Is not a reorder
		Exit Function
	End If

	Set oRsOrderDetails      = New cDataAccessRecordSet3
	Set oRsOrderDetailsCount = New cDataAccessRecordSet3
	Set oRSSingleProd        = New cDataAccessRecordSet3
	Set plReOrder            = New cPostLogic
	Set myFields             = Server.CreateObject("Scripting.Dictionary")
	Set aProdFields          = fncGetCartFields(PRODUCT_DEFAULT_VALUES)
	Set aCommentFields       = fncGetCartFields(COMMENT_DEFAULT_VALUES)

	sKey                     = Trim(Request.QueryString("key") & "")
	sQty                     = Trim(Request.QueryString("qty") & "")
	sProdKeysQty             = "" 'key~qty,key~qty,key~qty' for an easy way to get the qty ordered since we don't have post data
	sProdKeys                = "" 'key,key,key' to pass into validateOrderDetails
	sOKey                    = uSession.SC_ID
	iODCount                 = 0
	sProdWarehouse			 = ""

	Call myFields.Add("qty", "qty")

	If sKey <> "" And sQType = "reorder" Then
		'If we have a key, and no quantity, then it is a sales order key.
		oRsOrderDetails.SQL_ID                        = "Sales_Order_Detail"
		oRsOrderDetails.SQL_ID_Minor_VersionNumber    = 0
		oRsOrderDetails.SQL_ID_Revision_VersionNumber = 6
		oRsOrderDetails.SearchString                  = "searchexact~eosoh_id~" & sKey & "|AND|searchright~eosod.ref_id~1~0"
		oRsOrderDetails.ExtraFields                   = "p.p_key as p_key, p.status as p_status, ISNULL(eosod.qty_ordered,0) as qty_ordered, " &_
		                                                "CASE CASE WHEN IsNull(p.inv_item, 1) = 0 THEN 'noninv' WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN(IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END WHEN 'noninv' THEN '1' WHEN 'in' THEN CASE WHEN IsNull(idp.in_stock_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'onorder' THEN CASE WHEN IsNull(idp.on_order_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'out' THEN CASE WHEN IsNull(idp.out_of_stock_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END END AS calc_inv_show_cart, ISNULL(od.pw_id, 'AB5605934DCA4BBAA54C377CC04B624B') AS od_pw_id"
		Call oRsOrderDetails.AddDynamicJoin("LEFT JOIN inventory_display_profiles idp ON idp_key = p.idp_id")

		'Task ##### - avalis: Reorder Functionality wasn't working quite right. We need to pass in the pw_id. 
		Call oRsOrderDetails.AddDynamicJoin("LEFT JOIN external_order_sales_order_header eosoh ON eosoh.eosoh_key = eosod.eosoh_id")
		Call oRsOrderDetails.AddDynamicJoin("LEFT JOIN orders o ON o.c_ordNUM = eosoh.website_ordnum AND eosoh.order_date = o.effective_order_date")
		Call oRsOrderDetails.AddDynamicJoin("LEFT JOIN order_detail od ON od.o_id = o.o_key")

		Call oRsOrderDetails.Open()

		If oRsOrderDetails.EOF Then
			Response.Redirect "showcart.asp?proderr=1"
			Exit Function
		Else 
			Do While Not oRsOrderDetails.EOF
				'Do the status check here, not in the query, to save a DB call getting the count of products on the OD line.
				If (Trim(oRsOrderDetails("p_status")&"") = "1" Or Trim(oRsOrderDetails("p_status")&"") = "") _
				   And CBool(Trim(oRsOrderDetails("calc_inv_show_cart")&""))  Then

					If Len(sProdKeysQty) > 0 Then
						sProdKeysQty = sProdKeysQty & ","
						sProdKeys = sProdKeys & ","
					End If

					sProdKeysQty = sProdKeysQty  & oRsOrderDetails("p_key") & "~" & oRsOrderDetails("qty_ordered") & "~" & oRsOrderDetails("od_pw_id")
					sProdKeys = sProdKeys  & oRsOrderDetails("p_key")
					
					iODCount = iODCount + 1
				Else
					bProdErr = True
				End If ' If oRsOrderDetails("p_status") <> "0" Then
				oRsOrderDetails.MoveNext
			Loop
		End If 'If Not oRsOrderDetails.EOF Then

		If iODCount = 0 Then
			bProdErr = True
			Response.Redirect "showcart.asp?proderr=1"
			Exit Function
		End If
	ElseIf sKey <> "" And sQty <> ""  And Trim(Request.Querystring("pw_id") & "") <> "" And sQType = "qsaddprod" Then
		Dim sWarehouse
		sWarehouse = Request.QueryString("pw_id")
		'If we have a key and a quantity, then it is an individual product.
		oRSSingleProd.SQL_ID                        = "Base_Products"
		oRSSingleProd.SQL_ID_Minor_VersionNumber    = 0
		oRSSingleProd.SQL_ID_Revision_VersionNumber = 9
		oRSSingleProd.SearchString                  = "(searchexact~p.status~1|OR|isnull~p.status)|AND|searchexact~p.p_key~" & sKey
		oRSSingleProd.ExtraFields                   = "p.p_key as p_key, p.status as p_status, " &_
		                                              "CASE CASE WHEN IsNull(p.inv_item, 1) = 0 THEN 'noninv' WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN(IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END WHEN 'noninv' THEN '1' WHEN 'in' THEN CASE WHEN IsNull(idp.in_stock_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'onorder' THEN CASE WHEN IsNull(idp.on_order_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'out' THEN CASE WHEN IsNull(idp.out_of_stock_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END END AS calc_inv_show_cart"
		Call oRSSingleProd.AddDynamicJoin("LEFT JOIN inventory_display_profiles idp ON idp_key = p.idp_id")

		Call oRSSingleProd.Open()
		
		If Not oRSSingleProd.EOF Then
			If Cbool(Trim(oRSSingleProd("calc_inv_show_cart")&"")) Then
				iODCount = 1
				sProdKeysQty = sKey & "~" & sQty & "~" & sWarehouse
				sProdKeys = sKey
			Else
				bProdErr = True
				Response.Redirect "showcart.asp?proderr=1"
				Exit Function
			End If
			Response.Write "oRSSingleProd(p_key): " & oRSSingleProd("p_key") & "<br>"
		Else
			bProdErr = True
			Response.Redirect "showcart.asp?proderr=1"
			Exit Function
		End If 'If Not oRSSingleProd.EOF Then
		
		Set oRSSingleProd = Nothing

	ElseIf sQType = "relatedaddcart" and Request.Querystring("key") <> "" and Request.QueryString("qty")  <> "" Then 'This is for the add-on prods
		Dim sRelatedPKey, iRelatedQty
		sRelatedPKey = Request.Querystring("key")
		iRelatedQty = Request.QueryString("qty")
		'If we have a key and a quantity, then it is an individual product.
		oRSSingleProd.SQL_ID                        = "Base_Products"
		oRSSingleProd.SQL_ID_Minor_VersionNumber    = 0
		oRSSingleProd.SQL_ID_Revision_VersionNumber = 9
		oRSSingleProd.SearchString                  = "(searchexact~p.status~1|OR|isnull~p.status)|AND|searchexact~p.p_key~" & sRelatedPKey
		oRSSingleProd.ExtraFields                   = "p.p_key as p_key, p.status as p_status, " &_
		                                              "CASE CASE WHEN IsNull(p.inv_item, 1) = 0 THEN 'noninv' WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN(IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END WHEN 'noninv' THEN '1' WHEN 'in' THEN CASE WHEN IsNull(idp.in_stock_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'onorder' THEN CASE WHEN IsNull(idp.on_order_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'out' THEN CASE WHEN IsNull(idp.out_of_stock_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END END AS calc_inv_show_cart"
		Call oRSSingleProd.AddDynamicJoin("LEFT JOIN inventory_display_profiles idp ON idp_key = p.idp_id")

		Call oRSSingleProd.Open()
		
		If Not oRSSingleProd.EOF Then
			If Cbool(Trim(oRSSingleProd("calc_inv_show_cart")&"")) Then
				iODCount = 1
				sProdKeysQty = sRelatedPKey & "~" & iRelatedQty & "~AB5605934DCA4BBAA54C377CC04B624B" 'p_key~qty harcoded to 1~Warehouse 99 hardcoded
				sProdKeys = sRelatedPKey
			Else
				bProdErr = True
				Response.Redirect "showcart.asp?proderr=1"
				Exit Function
			End If
			Response.Write "oRSSingleProd(p_key): " & oRSSingleProd("p_key") & "<br>"
		Else
			bProdErr = True
			Response.Redirect "showcart.asp?proderr=1"
			Exit Function
		End If 'If Not oRSSingleProd.EOF Then
		
		Set oRSSingleProd = Nothing
	Else
		'Else we can just bail
		Exit Function
	End If

	'- Before posting the lines.. see if the header exists
	Call subCreateOrderHeader(sOKey, True)

	plReOrder.ImportTable = "order_detail"
	Call plReOrder.LoadFormPost()
	Call DrawDebugline("Session: " & uSession.SessionKey)
	Call DrawDebugline("Loaded [order_detail] Form Post")

	If Trim(sProdKeys & "") <> "" Then '- ADDs - Must pass this
		aKeys = Split(sProdKeys, ",")
	Else
		Response.Write "<!-- INVALID POST -->"
		Response.End
	End If '- If Trim(plReOrder.GetPostedValue("keys") & "") <> "" Then

	Call DrawDebugline("New cPostLogicData")
	Set plData = New cPostLogicData

	' Adding cart line counter - need this for proper ordering of cart contents with BOM and Prod Config.
	' Can't use Instance ordering - causes cart items to appear in a random order when added to the cart
	' rather than at the "bottom". Bad user experience and confusing.
	iCartLineCount = CInt(GetMaxCartLineNum(sOKey))

	'*
	' kbrown 2.26.2014 - #43460
	' Includes product order flags on order
	If CBool(USE_ORDER_FLAGS) Then
		sOrderFlagFields = ""

		'Get product XML
		sProductXML = plReOrder.GetImportFileXML("products")
		Set oProductXML = Server.CreateObject("Msxml2.DOMDocument")
		oProductXML.LoadXml(sProductXML)

		For Each field In oProductXML.documentElement.selectNodes("Columns/DatabaseTableColumnDetails")
			sFieldName = field.selectSingleNode("Name").Text
			If InStr(1, sFieldName, "of_", 1) = 1  Then
				sOrderFlagFields =  sOrderFlagFields & "|" & sFieldName  & "~0~" & "p." & sFieldName & "~"
			End If
		Next

		Set aProdFields = fncGetCartFields(PRODUCT_DEFAULT_VALUES & sOrderFlagFields)
	End If 'If CBool(USE_ORDER_FLAGS)

	Call validateOrderDetails(aKeys, sOKey)

	'*
	 'NOTE: ALL COLUMNS POSTED ON THE FIRST ROW in the PL, HAVE TO BE POSTED ON ALL (that's ALL) subsequent rows - cainb 11/23/2009
	 '		- I am forcing this with the dictionary solution (.. and I have to froce this because that is how our Post Logic object works)
	 '*
	aProdKeysQty = Split(sProdKeysQty, ",")

	For Each sData In aProdKeysQty
		aData = Split(sData, "~")
		sKey  = Trim(aData(0)&"")
		iQty  = Trim(aData(1)&"")
		sWarehouse = Trim(aData(2)&"")

		iCartLineCount = iCartLineCount + 1

		Call DrawDebugline("=================================")
		Call DrawDebugline("Starting Qty: " & iQty)

		If iQty & "" <> "" Then '- List VIews post ALL keys... but not QTYs
			If Not isReallyNumeric(iQty) Then
				iQty = 1
			End If

			bHitOne = True

			Call plData.AddRow()

			sCurODKey                         = CreateGUID()
			plData.ColumnValue("od_key")      = sCurODKey
			plData.ColumnValue("p_id")        = sKey
			plData.ColumnValue("o_id")        = sOKey
			plData.ColumnValue("date")        = Now()
			plData.ColumnValue("qty")         = "0"
			plData.ColumnValue("ws_id")       = WS_ID
			plData.ColumnValue("line_number") = iCartLineCount
			plData.ColumnValue("pw_id") 	  = sWarehouse

			'- This has to be checked because if the first row in the Post Logic doesn't set these then none will.
			If CBool(Request.Form("show_config_sections")) Then
				plData.ColumnValue("cs_id") = ""
				plData.ColumnValue("cc_id") = ""
				plData.ColumnValue("nm")    = ""
			End If

			'- Loop the list of fields in myFields. No need to Check for blank values...(the defaults get set and override blanks anyway)
			For Each sField In myFields
				sField = LCase(Trim(sField & ""))

				Select Case sField
					Case "qty"
						If iQty = 0 Then
							plData.ColumnValue("pending_removal") = 1
						Else
							plData.ColumnValue("qty_display") = iQty
							plData.ColumnValue("qty") = iQty
						End If

						Call DrawDebugline("Posting: " & sKey & ", Qty: " & iQty)

					Case "qty_display" '- if this isn't posted, then you can't get proper calcs on UOM.. BUT you must provide this HTML input AFTER the qty input! :) cainb 5/6/2013
						If plReOrder.getPostedValue(sField & "_" & sKey)&""="" Then
							plData.ColumnValue(sField) = plReOrder.getPostedValue("qty_" & sKey)
						Else
							plData.ColumnValue(sField) = plReOrder.getPostedValue(sField & "_" & sKey)
						End If

					Case Else
						plData.ColumnValue(sField) = plReOrder.getPostedValue(sField & "_" & sKey)

				End Select '- Select Case sField

				bFirstField = False
			Next '- For Each sField In myFields

			If plReOrder.GetPostedValue("type_" & sKey) = "comment" Then
				Set rsProduct = BuildProductRecordSet(sKey, aCommentFields)

				If Not rsProduct.EOF then
					Call setFieldDefaults(plData, aCommentFields, sKey, rsProduct)
				Else
					'Prod out of stock or disabled.
					Response.Redirect "showcart.asp?err=noqty"
				End If
			Else
				Set rsProduct = BuildProductRecordSet(sKey, aProdFields)


				If Not rsProduct.EOF Then
					Call setFieldDefaults(plData, aProdFields, sKey, rsProduct)

					' twalters - [3/10/2014] - Adding support for Product Configurator(Form Builder)
					If CBool(rsProduct("using_configuration_questions")) Then
						Set oConfigATC = New ConfiguratorAddToCart

						oConfigATC.ProductKey  = sKey
						oConfigATC.OrderKey    = sOKey
						oConfigATC.InstanceKey = plData.ColumnValue("instance")
						oConfigATC.ParentODKey = sCurODKey
						oConfigATC.GroupID     = iCartLineCount

						sCartOptions = oConfigATC.AddConfiguratorToCart(iQty, plReOrder, plData)
						sConfigJSON = oConfigATC.ConfigJSON

						'Its possible for the configurator to add multiple stand alone products to the cart so
						'we need to get the final count back from the configurator
						iCartLineCount = CInt(oConfigATC.FinalLineNumber())
						bIsProdConfig = True
					End If

					'- cainb - 12/23/2013 - adding BOM (show_config_sections)
					If CBool(rsProduct("show_config_sections")) Then
						plData.ColumnValue("remove_type")      = "instance" 'the object doesn't reliably do a tolower, so this needs to be lowercase.
						plData.ColumnValue("row_display_case") = "group-parent"
						plData.ColumnValue("line_number")      = iCartLineCount
						subAddSectionChoices sCurODKey, sKey, iQty, plData.ColumnValue("instance"), plData, rsProduct, iCartLineCount
					End If

					If CBool(GLOBAL_USE_EXPECTED_SHIP_DATE) Then
						sDate = CalculateExpectedShipDate(sKey)

						If IsDate(sDate) Then
							plData.ColumnValue("expected_date") = sDate
						End If
					End If
				Else
					'Prod out of stock or disabled.
					Response.Redirect "showcart.asp?err=noqty"
				End If
			End If '- If plReOrder.getPostedValue(sUnq & "_type") = "comment" Then
		End If '- Qty <> ""
	Next '- For Each sUnq In aUnqs

	Set plReOrder.PostLogicData = plData

	If bHitOne Then
		plReOrder.TableKeyField        = "od_key"
		plReOrder.UserKeyField         = "od_key"
		plReOrder.UserKeyIsPrimaryKey	= True
		plReOrder.UsingUserKey			= False

		DrawDebugline("plReOrder.ImportData()")
		Call plReOrder.ImportData()
		DrawDebugline("~plReOrder.ImportData()")

		If bIsProdConfig Then 'There should only be one of these posted
			Call updateCartOptions(sCurODKey, sOKey, sCartOptions, sConfigJSON)
		End If
	Else
		Response.Redirect "showcart.asp?err=noqty"
	End if

    '- Garbage collection
	myFields.RemoveAll
	Set myFields = Nothing
	Set plData   = Nothing
	Set plReOrder   = Nothing
	Set oRsOrderDetails = Nothing

	If CBool(bProdErr) Then
		'Then one or more prods are out of stock or disabled.'
		Response.Redirect "showcart.asp?proderr=1"
	Else
		Response.Redirect "showcart.asp"
	End If
End Function

Function DrawProductErr(aArgs)
	If CBool(Trim(Request.QueryString("proderr")&"")) Then
		response.write "<div class=""alert alert-error text-center""><b>One or more products added to your cart are disabled or out of stock.</b></div>"
	End If
End Function 'DrawProductErr

%>