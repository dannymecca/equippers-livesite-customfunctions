/**
 * custom logic for equippers:
 * if customer changes account billing info, reset tax exemption status.
 */
function editAccountInfo() {
    var $ = jQuery;
    $('#tax_status').val("1");
    $('#account-address-container').addClass('hide');
    $('#account-edit-container').removeClass('hide');

    toggleTaxExemptGlobalDisplay(false);
}


/**
 * There are two records for each global address: one that's tax-exempt and
 * one that's not. Each exempt version is printed to the page only if the
 * account is marked exempt in that state. However, ALL non-exempt records are
 * written to the page -- even those for which a tax-exempt record was also
 * returned.
 *
 *
 * The reason for this is that if the user changes their billing information,
 * they lose their tax exempt status (see function editAccountInfo), so we
 * need to show and hide the addresses based on the exemption status.
 *
 * At all times, only one record for each location should be shown:
 *
 * - On page load, we need to hide all non-exempt addresses in states where the
 *   account is marked tax-exempt.
 * - If the billing information is changed, hide all exempt addresses and show
 *   all non-exempt ones so that the customer is no longer able to select a
 *   tax-exempt address.
 *
 *  [dcohen @ 2016-11-23] #103359
 */

function toggleTaxExemptGlobalDisplay(bExempt){
    var $ = jQuery;

    var $container = $("fieldset#PickingAddressList");
    if(bExempt){
        // on page load, hide the non-exempt records were an exempt record exists
        if(window.oBillShipInfo.exempt_states !== ''){
            var aExemptStates = window.oBillShipInfo.exempt_states.split(',');

            var nonExemptSelector = aExemptStates.map(function(state){
                return "input[type=radio][name=sha_key][data-tax_exempt=false][data-s_state=" + state + "]";
            }).join(',');

            // show all tax-exempt addresses (just in case they're not shown for some reason)
            $("input[type=radio][name=sha_key][data-tax_exempt=true]", $container).removeAttr('disabled').parents('tr').removeClass('hide');
            // hide and disable the non-exempt ones.
            $(nonExemptSelector, $container).attr('disabled','disabled').parents('tr').addClass('hide');
        }
    } else {
        // account is NOT tax-exempt. Hide and disable all exempt addresses and show non-exempt ones.
        $("input[type=radio][name=sha_key][data-tax_exempt=false]", $container).removeAttr('disabled').parents('tr').removeClass('hide');
        $("input[type=radio][name=sha_key][data-tax_exempt=true]", $container).attr('disabled','disabled').parents('tr').addClass('hide');
    }

    // select an option if one isn't already selected (or if the selected option is no longer visible)
    var $selectedOption = $('input[type=radio][name=sha_key]:checked', $container);

    if(!$selectedOption.length){
        // if no option selected, auto-select the first one
        $("input[type=radio][name=sha_key]:enabled:first", $container).attr('checked','checked');
    } else if ($selectedOption.is(":disabled")){
        // if there was an option selected but it's no longer enabled/visible, find its enabled
        // counterpart and select that one instead.

        $("input[type=radio]:enabled[name=sha_key][data-sacct_code=" + $selectedOption.data('sacct_code') + ']', $container).attr('checked','checked');
    }

}

// ON PAGE LOAD: if the account is tax-exempt, show/hide the addresses as appropriate.
toggleTaxExemptGlobalDisplay(typeof window.oBillShipInfo !== 'undefined' && window.oBillShipInfo.tax_exempt === '1');
