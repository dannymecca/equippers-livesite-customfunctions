<%
'dcarton #119739 5/18/2017	
Function DrawTransactionColumns(aArgs)
	Set oColumn            = New cDDTColumn
	oColumn.Name           = "Message"
	oColumn.Field          = "responsexml"
	oColumn.IsSearchable   = False
	oColumn.IsSortable     = False
	oColumn.CustomRenderer = "MessageCustomRenderer"
	oColumn.DataType       = "customrender" 
	Call oDDT.AddColumn(oColumn)

	Set oColumn            = New cDDTColumn
	oColumn.Name           = "Bank Message"
	oColumn.Field          = "responsexml"
	oColumn.IsSearchable   = False
	oColumn.IsSortable     = False
	oColumn.CustomRenderer = "BankMessageCustomRenderer"
	oColumn.DataType       = "customrender" 
	Call oDDT.AddColumn(oColumn)

	Set oColumn            = New cDDTColumn
	oColumn.Name           = "AVS"
	oColumn.Field          = "responsexml"
	oColumn.IsSearchable   = False
	oColumn.IsSortable     = False
	oColumn.CustomRenderer = "AVSCustomRenderer"
	oColumn.DataType       = "customrender" 
	Call oDDT.AddColumn(oColumn)

	Set oColumn            = New cDDTColumn
	oColumn.Name           = "CVV2"
	oColumn.Field          = "responsexml"
	oColumn.IsSearchable   = False
	oColumn.IsSortable     = False
	oColumn.CustomRenderer = "CVV2CustomRenderer"
	oColumn.DataType       = "customrender" 
	Call oDDT.AddColumn(oColumn)


End Function 'DrawTransactionColumns

Private Function MessageCustomRenderer(oRsDDT, oCol)
	Dim sReturn
	'- get the import settings for virtual fields
	Set xmlConfig = Server.CreateObject("Microsoft.XMLDOM")
	
	If TRIM(oRsDDT("responsexml") & "") <> "" Then
		'- load the table column config xml
		xmlConfig.LoadXML(oRsDDT("responsexml"))
		
		'- get the import type from the table column config xml
		Set xmlImportType = xmlConfig.documentElement.SelectSingleNode("EXact_Message")
		
		If Not xmlImportType Is Nothing Then
			sReturn = Trim(xmlImportType.text&"")
		End If
	Else
		sReturn = "N/A"
	End If

	MessageCustomRenderer = sReturn
End Function'MessageCustomRenderer

Private Function BankMessageCustomRenderer(oRsDDT, oCol)
	Dim sReturn
	'- get the import settings for virtual fields
	Set xmlConfig = Server.CreateObject("Microsoft.XMLDOM")
	
	If TRIM(oRsDDT("responsexml") & "") <> "" Then
		'- load the table column config xml
		xmlConfig.LoadXML(oRsDDT("responsexml"))
		
		'- get the import type from the table column config xml
		Set xmlImportType = xmlConfig.documentElement.SelectSingleNode("Bank_Message")
		
		If Not xmlImportType Is Nothing Then
			sReturn = Trim(xmlImportType.text&"")
		End If
	Else
		sReturn = "N/A"
	End If

	BankMessageCustomRenderer = sReturn
End Function'BankMessageCustomRenderer

Private Function AVSCustomRenderer(oRsDDT, oCol)
	Dim sReturn
	'- get the import settings for virtual fields
	Set xmlConfig = Server.CreateObject("Microsoft.XMLDOM")
	
	If TRIM(oRsDDT("responsexml") & "") <> "" Then
		'- load the table column config xml
		xmlConfig.LoadXML(oRsDDT("responsexml"))
		
		'- get the import type from the table column config xml
		Set xmlImportType = xmlConfig.documentElement.SelectSingleNode("AVS")
		
		If Not xmlImportType Is Nothing Then
			sReturn = Trim(xmlImportType.text&"")
		End If
	Else
		sReturn = "N/A"
	End If

	AVSCustomRenderer = sReturn
End Function'AVSCustomRenderer

Private Function CVV2CustomRenderer(oRsDDT, oCol)
	Dim sReturn
	'- get the import settings for virtual fields
	Set xmlConfig = Server.CreateObject("Microsoft.XMLDOM")
	
	If TRIM(oRsDDT("responsexml") & "") <> "" Then
		'- load the table column config xml
		xmlConfig.LoadXML(oRsDDT("responsexml"))
		
		'- get the import type from the table column config xml
		Set xmlImportType = xmlConfig.documentElement.SelectSingleNode("CVV2")
		
		If Not xmlImportType Is Nothing Then
			sReturn = Trim(xmlImportType.text&"")
		End If
	Else
		sReturn = "N/A"
	End If

	CVV2CustomRenderer = sReturn
End Function'CVV2CustomRenderer


%> 