<%

'**
 ' This function overrides the decimal places returned
 ' for items ending with .00
 ' @since 7/24/2014
 ' @author jasonb
 ' @param aArgs(1) m_ors from Site Config 
 ' @param Val getPrice value from PO object
 ' @return getPrice
'**
Public Function fncDataAccess3FilterGetPrice(aArgs, Val)


	Dim getPrice : getPrice = Val

	If Right(getPrice,3) = ".00" Then
		getPrice = formatcurrency(getPrice,0)
	Else
		getPrice = formatcurrency(getPrice,GLOBAL_UNIT_PRICE_DECIMAL_PLACES)
	End if

	fncDataAccess3FilterGetPrice = getPrice

End Function '-Public Function fncDataAccess3FilterGetPrice(aArgs, Val)

%>