<%
'**
 ' This function overrides the Draw Column Function in the 
 ' Cart builder Column Remove Class
 ' @since 10/6/2014
 ' @author jasonb
 ' @param aArgs(1) oRecordSet from CB 
 ' @param aArgs(2) oCartDisplay from CB
 ' @param aArgs(2) sMessage from CB
 ' @param Val formatmoney value from session function
 ' @return getPrice
'**
Public Function fncCartBuilderColumnRemoveDrawColumnAction(aArgs) 
	'- If remove is set to hide do not build the remove column.
	If LCase(Trim(aArgs(1)("sc_remove_type") & "")) = "hide" Then
		%><td>&nbsp;</td><%
		DrawColumn = False
		Exit Function
	End If '- If LCase(Trim(oRecordSet("sc_remove_type") & "")) = "hide" Then

	If aArgs(1)("sc_remove_ds") & "" <> "" Then
		Response.Write aArgs(1)("sc_remove_ds")
	Else
		%>
		<td class="cart-col-actions">
			<a href="#" onclick="return (gaTrackATC('<%=aArgs(1)("sc_sku")%>',<%=JSQ(Replace(aArgs(1)("p_nm"),"""","\x22"))%>,'<%=sCatName%>','','<%=aArgs(1)("sc_price")%>', '<%=aArgs(1)("sc_unq")%>' , 'remove') && fncConfirmRemove(this, 'Are you sure you wish to delete this item?'));return false;" id="remove_<%=aArgs(1)("sc_unq")%>" class="cart-action-delete tooltip-trigger" title="Delete item"><i class="icon-trash"></i></a>
		</td>
		<%
	End If '- If aArgs(1)("remove_ds") & "" <> "" Then

	DrawColumn = True
End Function

%>