<% 
' Web driver Updates 
' Product categories 
' - If opt2 = 1 then display as nextopia page on site front else load wsp page. 
' Search terms 
' - If opt1 is 1 then used for product page if 2 used for nextopia feed 
' Search groups 
' - If opt1 is 1 then used for product page if 2 used for nextopia feed 

Function fncAddCategoryQuestions(aArgs)
	Dim oF : Set oF = aArgs(1)
		
	oF.SectionHeader "Nextopia Options "
	
	Set oQuestion						= New Question_Radio
	oQuestion.Props.Name				= "opt2"
	oQuestion.Props.Question			= "Display as nextopia Category Page:"
	oQuestion.Props.Answers				= "Yes~1|No~0"
	oQuestion.Props.DefaultAnswers		= "0"
	oQuestion.Props.UseHelp				= True
	oQuestion.Props.HelpTitle			= Left(oQuestion.Props.Question, Len(oQuestion.Props.Question) - 1)
	oQuestion.Props.HelpText			= "Setting this to yes will display the nextopia js on the category page. Setting this to no will load the normal WSP category page."
	oF.AddQuestion						oQuestion


	oF.SectionHeader "Left Rail Content "

	Set oQuestion							= New Question_Text
	oQuestion.Props.Name				= "page_id"
	oQuestion.Props.Question			= "Left rail web page"
	oQuestion.Props.Size				= 41
	oQuestion.Props.UseFindLightbox		= True
	oQuestion.Props.UseHiddenLBField	= True
	oQuestion.Props.DisplayField		= "nm"
	oQuestion.Props.ValueField			= "p_key"
	oQuestion.Props.UseHelp				= True
	oQuestion.Props.HelpTitle			= Left(oQuestion.Props.Question, Len(oQuestion.Props.Question) - 1)
	oQuestion.Props.HelpText			= "Choose a web page to display as left rail content. Replaces Xigla content. Does not display on nextopia pages. "

		oQuestion.Props.LightBoxTitle							= "Find Web Page"
		oQuestion.Props.LightBoxDescription						= "Please select a web page."
		oQuestion.Props.LightBoxSQL_ID							= "Manage_Web_Pages"
		oQuestion.Props.LightBoxSQL_ID_Minor_VersionNumber		= 0
		oQuestion.Props.LightBoxSQL_ID_Revision_VersionNumber	= 4
		oQuestion.Props.LightBoxExtraFields						= "p.nm, p.page_alias"
		oQuestion.Props.LightBoxOrderBy							= "p.nm~asc"
		oQuestion.Props.LightBoxTableColumns					= "nm;Name~page_alias;Alias"
		oQuestion.Props.LightBoxPrimaryKey						= "p_key"
		oQuestion.Props.LightBoxBlankOnLoad						= False

	oF.AddQuestion						oQuestion

End Function 

Function fncSearchTermQuestions(aArgs)
	Dim oF : Set oF = aArgs(1)

	sOpt1DefaultVal = "2"
	If LCase(GetPageName) = "mgr_search_terms_detail_ae.asp" Then 
		sOpt1DefaultVal = "1"
	End If 

	Set oQuestion							= New Question_Text
	oQuestion.Props.Name					= "opt1"
	oQuestion.Props.QuestionType			= "hidden"
	oQuestion.Props.Question				= "Where search term is used? "
	oQuestion.Props.DefaultAnswers 			= sOpt1DefaultVal
	oF.AddQuestion  						oQuestion

End Function


Function fncUpdateSearchTermDetailManageDDT(aArgs)
	Dim oDDT : Set oDDT = aArgs(1)

	If LCase(GetPageName) = "mgr_search_terms_detail_man.asp" Then 
		oDDT.searchstring = "searchexact~std.opt1~1"
	Else
		oDDT.searchstring = "searchexact~std.opt1~2"
	End If
End function

Function fncUpdateSearchTermsManageDDT(aArgs)
	Dim oDDT : Set oDDT = aArgs(1)

	If LCase(GetPageName) = "mgr_search_terms_on_products_man.asp" Then 
		oDDT.searchstring = "searchexact~std.opt1~1"
	Else
		oDDT.searchstring = "searchexact~std.opt1~2"
	End If
End function 

Function fncUpdateSearchTermsOnProductAddEdit(aArgs)
	Dim oQuestion : Set oQuestion = aArgs(1)

	If LCase(GetPageName) = "mgr_search_terms_on_products_ae.asp" Then 
		oQuestion.Props.LightboxSearchString = "searchexact~std.opt1~1"
	Else
		oQuestion.Props.LightboxSearchString = "searchexact~std.opt1~2"
	End If
End Function  

Function fncUpdateSearchTermSearchGroupQuestion(aArgs)
	Dim oQuestion : Set oQuestion = aArgs(1)

	If LCase(GetPageName) = "mgr_search_terms_detail_ae.asp" Then 
		oQuestion.Props.LightboxSearchString = "searchexact~sg.opt1~1"
	Else
		oQuestion.Props.LightboxSearchString = "searchexact~sg.opt1~2"
	End If
End Function 


Function fncManageSearchGroupsDDT(aArgs)
	Dim oDDT : Set oDDT = aArgs(1)

	If LCase(GetPageName) = "mgr_search_groups_man.asp" Then 
		oDDT.searchstring = "searchexact~sg.opt1~1"
	Else
		oDDT.searchstring = "searchexact~sg.opt1~2"
	End If
End Function 

Function fncAddHiddenFieldstoSearchGroupAddEdit(aArgs)
	Dim oF : Set oF = aArgs(1)

	sOpt1DefaultVal = "2"
	If LCase(GetPageName) = "mgr_search_groups_ae.asp" Then 
		sOpt1DefaultVal = "1"
	End If 

	Set oQuestion							= New Question_Text
	oQuestion.Props.Name					= "opt1"
	oQuestion.Props.QuestionType			= "hidden"
	oQuestion.Props.Question				= "Where search term is used? "
	oQuestion.Props.DefaultAnswers 			= sOpt1DefaultVal
	oF.AddQuestion  						oQuestion

	If LCase(GetPageName) = "mgr_search_groups_nextopia_ae.asp" Then 
		' Allows website owner to specify where the search group displays. Using to determine between nextopia feed or product detail tab display. 
		Call oF.SectionHeader("Search Group Display Preferences: ")
		Set oQuestion							= New Question_Radio
		oQuestion.Props.Name					= "opt3"
		oQuestion.Props.Question				= "Where to display? "
		oQuestion.Props.DefaultAnswers 			= "both"
		oQuestion.Props.Answers 				="Nextopia~nextopia|Product Tabs~product_tabs|Both~both"
		oF.AddQuestion  						oQuestion
	End If 
End Function 
' End Web driver Updates 


' Feed Updates 

' Un comment when going live. 
Function fncAddNextopiaExtraFields(aArgs, sVal)
	sUrl = ORDERFRONTURL
	sExtraFields	= " p.sku as guid,"

	If Request.QueryString("feed_contents") = "facets" Then 
		sExtraFields = sExtraFields & " Replace(sg.nm, ' ', '')+'~'+std.nm as faceted_search_terms, sg.nm "

							'& " ( STUFF (( SELECT '|' + Replace(sg.nm,' ', '') + '~' + std.nm FROM products_search_term_details_map  AS pstdm " _ 
							'& 	"LEFT JOIN search_term_details AS std ON std.std_key = pstdm.std_id " _ 
							'& 	"LEFT JOIN dbo.search_group AS sg ON sg.sg_key = std.sg_id " _ 
							'& 	"WHERE pstdm.p_id = p.p_key AND sg.opt1 = '2' AND std.opt1 = '2' ORDER BY sg.nm DESC "_ 
							'& 	"FOR XML PATH('') " _ 
							'& 	"), 1, 1, '')) AS faceted_search_terms" 
	Else  
		sExtraFields = sExtraFields & "( select top 1 " _
								& " CASE po.calculation_adjustment" _
								& " WHEN 'percent_of' THEN" _
								& " CASE WHEN min(pb.adjustment) = max(pb.adjustment) THEN '' ELSE " _
								& " cast(convert(money, (p.retail_price * (min(pb.adjustment) / 100))) as varchar) + ' - '" _
								& " + cast(convert(money, (p.retail_price * (max(pb.adjustment) / 100))) as varchar) END" _
								& " WHEN 'set_price' THEN" _
								& " CASE WHEN min(adjustment) = max(adjustment) then '' ELSE " _
								& "  cast(convert(money, (min(adjustment))) as varchar) + ' - ' + cast(convert(money, max(adjustment)) as varchar) END" _
								& " ELSE cast(p.retail_price as varchar) END" _
							& " FROM pricing_overrides po" _
							& " INNER JOIN pricing_breaks pb ON pb.po_id = po_key" _
							& " WHERE po.p_id = p.p_key" _
							& " AND po.price_group='" & USERSESSIONS_CONTENT_DEFAULTPRICEGROUP & "' " _
							& " group by po.calculation_adjustment" _
							& ") as qty_price, " _

							& " p.nm as title," _ 
							& sPriceField & "," _
						
						& "isnull(p.searchfield6,'') as brand," _

						& "(CASE" _
							& " WHEN isnull(p.price_display_type, '') <>  '' THEN" _
							& " 'Sign in or go to checkout.' " _
							& " ELSE '' "_
						& " END) as price_message," _
						& "isnull(p.price_display_type, '') as price_display_type," _
						& "isnull(p.searchfield4,'') as color," _
						& "isnull(p.searchfield5,'') as size," _
						& "isnull(p.optional8, '') as uom," _
						& "isnull(p.searchfield2, '') as redLineText," _
						& "isnull(p.searchfield3, '') as feature," _
						& "isnull(p.opt5, '') as manufacturer_part," _
						& "isnull(p.searchfield9, '') as alternative_items," _ 
						& "isnull(sp.se_ds, '') as description," _
						& "isnull(sp.se_keywords, '') as keywords," _

						& "(CASE " _
							& "	WHEN sp.sp_key is not null THEN" _
							& "		'" & sUrl & "/' + sp.se_directory + '/' + sp.se_pagename + '" & QUERYSTRING_ATTRIBUTES & "'"_
							& "	ELSE" _
							& "		'" & sUrl & "/pc_product_detail.asp?key=' + p.p_key + '" & Replace(QUERYSTRING_ATTRIBUTES, "?", "&") & "'"_
						& " END) as link," _

						& "(CASE" _
							& "	WHEN IsNull(p.thumb,'') <> '' THEN" _
							& "		'" & sUrl & "/images/' + p.thumb" _
							& "	WHEN IsNull(p.pic,'') <> '' THEN" _
							& "		'" & sUrl & "/images/' + p.pic" _
							& "	ELSE" _
							& "		''" _
						& " END) as image_link," _
						& "(CASE" _
							& " WHEN IsNull(p.lg_pic, '') <> '' THEN" _
							& "		'" & sUrl & "/images/' + p.lg_pic" _
						& " END) as large_image," _

						& "(CASE " _
							& " WHEN isnull(p.flag1, '') = 1 THEN " _
							& "		'" & sUrl & "/images/flag1.png'" _
							& " ELSE" _
							& "		 ''" _
						& "END) as flag1," _
						& "(CASE " _
							& " WHEN isnull(p.flag2, '') = 1 THEN " _
							& "		'" & sUrl & "/images/flag2.png'" _
							& " ELSE" _
							& "		 ''" _
						& "END) as flag2," _
						& "(CASE " _
							& " WHEN isnull(p.flag3, '') = 1 THEN " _
							& "		'" & sUrl & "/images/flag3.png'" _
							& " ELSE" _
							& "		 ''" _
						& "END) as flag3," _
						& "(CASE " _
							& " WHEN isnull(p.flag4, '') = 1 THEN " _
							& "		'" & sUrl & "/images/flag4.png'" _
							& " ELSE" _
							& "		 ''" _
						& "END) as flag4," _

						& "stuff((" _
						& "SELECT '|' + Replace(Replace(Replace(Replace(Replace(pc.nm,'&',''),' ','-'),'""', '-'),'/', '-'),'''','-') " _
						& "FROM product_categories pc left join product_category_map pcm on pc.pc_key=pcm.pc_id " _
						& "WHERE pcm.p_id = p.p_key " _
						& "FOR XML path('') " _
						& "), 1, 1, '') as cats," _
						& "stuff((" _
						& "SELECT '|' + pc.pc_key " _
						& "FROM product_categories pc left join product_category_map pcm on pc.pc_key=pcm.pc_id " _
						& "WHERE pcm.p_id = p.p_key " _
						& "FOR XML path('') " _
						& "), 1, 1, '') as cat_keys," _

						& "stuff((" _
						& "SELECT '|' + p2.sku " _
						& "FROM products_suggested ps " _
						& "INNER JOIN products p2 On ps.s_p_id = p2.p_key " _
						& "WHERE ps.p_id = p.p_key " _
						& "FOR XML path('') " _
						& "), 1, 1, '') as suggested," _
						& " p.child_retail_price, p.child_skus, p.child_nms, p.parent_child_type, p.p_key as p_key, isnull(p.case_qty,1) as case_qty, p.optional13 as p_optional13" 

	End If 

	fncAddNextopiaExtraFields = sExtraFields
End Function



Public function fncUpdateNextopiaXmlNodes(aArgs)
	Dim oProductsRecordSet : Set oProductsRecordSet = aArgs(1)
	sInventoryMessage = ""
	If Request.QueryString("feed_contents") = "facets" Then 
		fncBuildOutFacetedSearchNodes(oProductsRecordSet)
	Else 
		Do While Not oProductsRecordSet.EOF
				Response.Write "<item>" & vbCrLf

					Response.write "<guid isPermaLink=""false"">" 			 	& XmlEncode( oProductsRecordSet("guid") ) 				& "</guid>" & vbCrLf

					'If Request.QueryString("feed_contents") = "facets" Then 
					'	Call fncBuildOutFacetedSearchNodes(oProductsRecordSet("faceted_search_terms"))
					'Else 
						If oProductsRecordSet.InventoryStatus = "in" Then 
							sInventoryMessage = "<span class=""inv_qty instock"">" & oProductsRecordSet.InventoryQuantity & "</span>" & oProductsRecordSet.InventoryMessage() 
						Else 
							sInventoryMessage = oProductsRecordSet.InventoryMessage() 
						End If 
						Response.Write "<title>" 			 	& XmlEncode( oProductsRecordSet("title") ) 			& "</title>" & vbCrLf
						Response.Write "<link>" 			 	& XmlEncode( oProductsRecordSet("link") ) 				& "</link>" & vbCrLf
						Response.Write "<description>" 		 	& XMLEncode( oProductsRecordSet("description") )  		& "</description>" & vbCrLf
						Response.Write "<image>" 			 	& XmlEncode( oProductsRecordSet("image_link") ) 		& "</image>" & vbCrLf
						Response.Write "<price>" 			 	& XmlEncode( oProductsRecordSet("price") ) 			& "</price>" & vbCrLf
						Response.Write "<uom>" 				 	& XmlEncode( oProductsRecordSet("uom") ) 				& "</uom>" & vbCrLf
						Response.Write "<price_message>" 	 	& XmlEncode( oProductsRecordSet("price_message") ) 	& "</price_message>" & vbCrLf
						Response.Write "<price_display_type>" 	& XmlEncode( oProductsRecordSet("price_display_type") ) 	& "</price_display_type>" & vbCrLf
						Response.Write "<qty_price>" 			& XmlEncode( oProductsRecordSet("qty_price") ) 		& "</qty_price>" & vbCrLf
						Response.Write "<manufacturer_part>"	& Trim( XmlEncode( oProductsRecordSet("manufacturer_part") ) ) & "</manufacturer_part>" & vbCrLf
						Response.Write "<alternative_items>" 	& XmlEncode( oProductsRecordSet("alternative_items") ) & "</alternative_items>" & vbCrLf
						Response.Write "<keywords>"  		 	& XmlEncode( oProductsRecordSet("keywords") ) 			& "</keywords>" & vbCrLf
						Response.Write "<related_products>"  	& XmlEncode( oProductsRecordSet("suggested") ) 		& "</related_products>" & vbCrLf
						Response.Write "<category>" 			& XmlEncode( oProductsRecordSet("cats") ) 				& "</category>" & vbCrLf
						Response.Write "<categorykeys>" 		& XmlEncode( oProductsRecordSet("cat_keys") ) 				& "</categorykeys>" & vbCrLf
						Response.Write "<brand>" 				& XmlEncode( oProductsRecordSet("brand") ) 			& "</brand>" & vbCrLf
						Response.Write "<color>" 				& XmlEncode( oProductsRecordSet("color") ) 			& "</color>" & vbCrLf
						Response.Write "<size>" 				& XmlEncode( oProductsRecordSet("size") ) 				& "</size>" & vbCrLf
						Response.Write "<red_line_txt>" 		& XmlEncode( oProductsRecordSet("redLineText") ) 				& "</red_line_txt>" & vbCrLf
						Response.Write "<feature>" 				& XmlEncode( oProductsRecordSet("feature") ) 			& "</feature>" & vbCrLf
						Response.Write "<large_image>" 			& XmlEncode( oProductsRecordSet("large_image") ) 		& "</large_image>" & vbCrLf
						Response.Write "<flag1>" 				& XmlEncode( oProductsRecordSet("flag1") ) 			& "</flag1>" & vbCrLf
						Response.Write "<flag2>" 				& XmlEncode( oProductsRecordSet("flag2") ) 			& "</flag2>" & vbCrLf
						Response.Write "<flag3>" 				& XmlEncode( oProductsRecordSet("flag3") ) 			& "</flag3>" & vbCrLf
						Response.Write "<flag4>" 				& XmlEncode( oProductsRecordSet("flag4") ) 			& "</flag4>" & vbCrLf
						Response.Write "<child_retail_price>" 	& XmlEncode( oProductsRecordSet("child_retail_price")) & "</child_retail_price>" & vbCrLf
						Response.Write "<child_skus>" 			& XmlEncode( oProductsRecordSet("child_skus") )		& "</child_skus>" & vbCrLf
						Response.Write "<child_nms>" 			& XmlEncode( oProductsRecordSet("child_nms") )			& "</child_nms>" & vbCrLf
						Response.Write "<parent_child_type>" 	& XmlEncode( oProductsRecordSet("parent_child_type") )	& "</parent_child_type>" & vbCrLf
						Response.Write "<p_key>" 				& XmlEncode( oProductsRecordSet("p_key") )				& "</p_key>" & vbCrLf
						Response.Write "<case_qty>" 			& XmlEncode( oProductsRecordSet("case_qty") )			& "</case_qty>" & vbCrLf
						Response.Write "<show_cart>" 			& XmlEncode( oProductsRecordSet.ShowCart(true))			& "</show_cart>" & vbCrLf
						Response.Write "<show_price>" 			& XmlEncode( oProductsRecordSet.ShowPrice(true))			& "</show_price>" & vbCrLf
						Response.Write "<show_inv>" 			& XmlEncode( oProductsRecordSet.ShowInventory(true))		& "</show_inv>" & vbCrLf
						Response.Write "<inventory_status>" 	& XmlEncode( oProductsRecordSet.InventoryStatus)		& "</inventory_status>" & vbCrLf
						Response.Write "<inv_msg>" 				& XmlEncode( sInventoryMessage)			& "</inv_msg>" & vbCrLf

						sInventoryMessage = ""
						If CBool(USE_PRODUCT_OPT_FIELDS_TABLE) Then
							DrawProductOptFields(MAX_PROD_OPT_FIELDS)
						End If

						If Trim(oRsProducts("p_optional13") & "") <> "" Then
			    			Response.Write "<optional13>" 			& XmlEncode( ORDERFRONTURL & "/images/" & oRsProducts("p_optional13") )			& "</optional13>" & vbCrLf
						End If

					'End If 
				Response.Write "</item>" & vbCrLf

			oProductsRecordSet.MoveNext 
		Loop '- Do While Not oProductsRecordSet.EOF
	End If 

End Function 
Public Function fncOverrideNextopiaDataAccess(aArgs)
	Dim oRsProducts : Set oRsProducts = aArgs(1)
	If Request.QueryString("feed_contents") = "facets" Then 
		oRsProducts.SQL_ID							= "base_products_search_term_details_map_without_key" 
		oRsProducts.SQL_ID_Minor_VersionNumber		= 0
		oRsProducts.SQL_ID_Revision_VersionNumber	= 2
		oRsProducts.UseDistinctQuery				= True 
		oRsProducts.UseIDP							= False
		oRsProducts.ProductPricingTableAlias 		= "p"

		If sLimit <> "" Then oRsProducts.MaxRecords	= sLimit

		oRsProducts.ExtraFields						= sExtraFields & sPricingFields & sProdOptFields
		sNextopiaProductFilter						= "searchexact~p.status~1" _
													& "|AND|isnullnot~p.status" _
													& "|AND|searchexact~p.searchfield13~1" _
													& "|AND|(searchexact~sg.opt3~both|OR|searchexact~sg.opt3~nextopia|OR|isnull~sg.opt3)"

		' Danny Mecca 6.22.16
		' Added filter for search string. Restructured so that 
		' sSingProdFilter can be set independent of hard coded product search string. 

		nextopia_feed_array 						= array("NextopiaProductFeedFilter")
		sNextopiaProductFilter 						= apply_filters(nextopia_feed_array, sNextopiaProductFilter)
		sNextopiaProductFilter 						= sNextopiaProductFilter & sSingleProdFilter
		
		oRsProducts.SearchString 					= sNextopiaProductFilter 

		Call oRsProducts.AddDynamicJoin				( "inner join search_term_details AS std ON std.std_key = pstdm.std_id and std.opt1 = '2' " )
		Call oRsProducts.AddDynamicJoin				( "inner JOIN search_group AS sg ON sg.sg_key = std.sg_id and sg.opt1 = '2'" )
		Call oRsProducts.AddDynamicJoin				( "inner join products as p on p.p_key = pstdm.p_id " )

		Call oRsProducts.AddOrderBy  ("p.sku, sg.nm", "ASC")


		oRsProducts.UseProductPricing				=  False
	Else 

		oRsProducts.SQL_ID							= "Nextopia_Product_Feed" 
		oRsProducts.SQL_ID_Minor_VersionNumber		= 0
		oRsProducts.SQL_ID_Revision_VersionNumber	= 2
		oRsProducts.UseDistinctQuery				= USE_DISTINCT_QUERY_FOR_FEED
		oRsProducts.UseIDP							= True
		oRsProducts.ProductPricingTableAlias 		= "p"

		If sLimit <> "" Then oRsProducts.MaxRecords	= sLimit

		oRsProducts.ExtraFields						= sExtraFields & sPricingFields & sProdOptFields
		sNextopiaProductFilter						= "searchexact~p.status~1" _
													& "|AND|isnullnot~p.status" _
													& "|AND|searchexact~p.searchfield13~1" 
		' Danny Mecca 6.22.16
		' Added filter for search string. Restructured so that 
		' sSingProdFilter can be set independent of hard coded product search string. 

		nextopia_feed_array 						= array("NextopiaProductFeedFilter")
		sNextopiaProductFilter 						= apply_filters(nextopia_feed_array, sNextopiaProductFilter)
		sNextopiaProductFilter 						= sNextopiaProductFilter & sSingleProdFilter
		
		oRsProducts.SearchString 					= sNextopiaProductFilter 

		Call oRsProducts.AddDynamicJoin				( "LEFT JOIN static_pages sp ON p.p_key = sp.content_id" )
		Call oRsProducts.AddDynamicJoin				( "LEFT JOIN products parent ON p.parent_p_id = parent.p_key" )


		oRsProducts.UseProductPricing				= CBool(USE_LOWEST_BREAK_PRICE)
	End If 

End Function 


' Vars 
' sTempSku  = temp sku for each item node. Changes when sku in list does not match last sku. 
' sTempFacetGroup = temp group for each group inside of item node. Group changes when next element in list does not have same group. 
' sFacetedTerms = temp string of terms associated with group. Value is reset after group is completed. 
Function fncBuildOutFacetedSearchNodes(oProductsRecordSet)
	sTempSku = ""
	sTempFacetGroup =""
	Dim aFacetTermInitArray(1)
	Dim sFacetedTerms
	
	Response.Write "<item>" & vbCrLf
	Response.write "<guid isPermaLink=""false"">" 			 	& XmlEncode( oProductsRecordSet("guid") ) 				& "</guid>" & vbCrLf
	sTempSku = oProductsRecordSet("guid")

	sTempFacetGroup = Split(oProductsRecordSet("faceted_search_terms"), "~")(0)

	Do While Not oProductsRecordSet.EOF
		

		If sTempSku <> oProductsRecordSet("guid") Then 
			Response.Write "<" & sTempFacetGroup & ">"  & sFacetedTerms & "</" & sTempFacetGroup & ">" 	& vbCrLf
			sFacetedTerms = ""
			Response.Write "</item>" & vbCrLf
			Response.Write "<item>" & vbCrLf
			Response.write "<guid isPermaLink=""false"">" 			 	& XmlEncode( oProductsRecordSet("guid") ) 				& "</guid>" & vbCrLf
		End If 
		
		sFacetedSearchString = oProductsRecordSet("faceted_search_terms")

		If Instr(sFacetedSearchString, "~") Then

		    	If sTempFacetGroup <> Split(sFacetedSearchString, "~")(0) and sTempSku = oProductsRecordSet("guid")  Then 
		    		Response.Write "<" & sTempFacetGroup & ">"  & sFacetedTerms & "</" & sTempFacetGroup & ">" 	& vbCrLf

		    		sFacetedTerms =  XmlEncode( Split(sFacetedSearchString, "~")(1) )
		    	Else 

		    		If sFacetedTerms = "" Then 
		    			' set sFacetedTerms to new term so || not added to beginning of string. 
		    			sFacetedTerms = XmlEncode( Split(sFacetedSearchString, "~")(1) )
		    		Else 
		    			sFacetedTerms = sFacetedTerms & "||" & XmlEncode( Split(sFacetedSearchString, "~")(1) )
		    		End If 
		    		
		    	End If 
		    	sTempFacetGroup = Split(sFacetedSearchString, "~")(0) 
		    'End If
	    End If

	    sTempSku = oProductsRecordSet("guid")
	    
	    oProductsRecordSet.MoveNext 
	Loop 

	If sFacetedTerms <> "" Then 
		Response.Write "<" & sTempFacetGroup & ">"  & sFacetedTerms & "</" & sTempFacetGroup & ">" 	& vbCrLf
	End If 

	Response.Write "</item>" & vbCrLf

End Function
%> 