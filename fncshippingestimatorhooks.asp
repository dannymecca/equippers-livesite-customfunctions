<%

Public Function UpdateShippingMethod(aArgs)
    Dim oShipMethod
    Set oShipMethod = aArgs(1)

	oShipMethod.ExcludeFreight 			= CBool(EXCLUDE_FREIGHT_WHEN_NO_FREIGHTPRODS)
	oShipMethod.UseFirstFreightOnly 	= CBool(USE_FIRST_FREIGHT_ONLY)
	oShipMethod.ShipToRequiresLiftgate 	= True '- DEFAULT to TRUE for ALL EQUIPPERS Estimations
End Function

Public Function DrawFreightMessage(aArgs)
	%><div id="freight_msg" class="freight_msg" style="display:none;">Includes Liftgate &amp; Call Ahead Delivery</div><%
End Function

Public Function UpdateShipViaRecordset(aArgs)
	Dim rsShipEstimates
	Set rsShipEstimates = aArgs(1)
	rsShipEstimates.ExtraFields = Replace(rsShipEstimates.ExtraFields, "svc.ref_id", "svc.sv_ref_id as ref_id")
	rsShipEstimates.ExtraFields = rsShipEstimates.ExtraFields  & ", sv.delivery_type"
	'rsShipEstimates.AddDynamicJoin "INNER JOIN ship_vias sv on sv.sv_key = svc.sv_id"
End Function

'rmaas 10/17/2016
'Ported code from Shipping Estimator - EQUIPPERS
Public Function UpdateShippingEstimatorXML(aArgs, sValue)
	Dim objRoot
	Dim oFilterXML
	Dim oSTXML
	Dim oShippingMethod
	Dim sRefId
	Dim bCheapestLTLMethodFound
	Dim bFirst: bFirst = True
	Dim bHasFreightProducts : bHasFreightProducts = False
	Set oSTXML = Server.CreateObject("Microsoft.XMLDOM")
	Dim rsCart

	'check if freight is required'
	Set rsCart                           = New cDataAccessRecordSet3
	rsCart.SQL_ID_Minor_VersionNumber    = 0
	rsCart.SQL_ID_Revision_VersionNumber = 0
	rsCart.SQL_ID                        = "Base_Order_Table_Cart"
	rsCart.SearchString                  = "searchexact~o_id~" & uSession.SC_ID
	rsCart.ExtraFields                   = "p.freight_required"
	rsCart.AddDynamicJoin 				 "INNER JOIN products p on p.p_key = p_id"

	Call rsCart.Open()

	Do while Not rsCart.EOF
		If rsCart("freight_required") = "1" Then
				bHasFreightProducts = True
			Exit do
		End If
		Call rsCart.MoveNext()
	Loop
	'Set rsCart = Nothing

	If oSTXML.LoadXML(sValue) Then
		Set oShipVia = oSTXML.GetElementsByTagName("ShippingMethods")

		Set oFilterXML = Server.CreateObject("Microsoft.XMLDOM")

		oFilterXML.loadxml "<?xml version=""1.0"" encoding=""UTF-8"" ?>"

		Set objRoot = oFilterXML.createElement("ArrayOfShippingMethods")
		oFilterXML.appendChild objRoot

		If oShipVia.Length > 0 Then
			bCheapestLTLMethodFound = False
			For Each oShippingMethod In oShipVia
				sDeliveryType = LCase(oShippingMethod.SelectSingleNode("DeliveryType").Text)
				sRefId = LCase(oShippingMethod.SelectSingleNode("RefID").Text)
				If bHasFreightProducts Then
					If UCase(Left(sRefId, 3)) = "LTL" Or sDeliveryType="other" Then
						objRoot.appendchild(oShippingMethod)
						Exit For
					End If
				Else
					If UCase(Left(sRefId, 3)) = "LTL" Then
						If bFirst then
							objRoot.appendchild(oShippingMethod)
							bFirst = False
							Exit For '- if first method is LTL, then return it only when NO Freight Products
						End if
					Else 'If sDeliveryType = "standard" Then
						If sRefId = "failover-ship-via" Then

						Else
							objRoot.appendchild(oShippingMethod)
							bFirst = False
						End If
					End If '-If sDeliveryType = "ltl" Then
				End If
			Next
		End If ' If oShipVia.Length > 0 Then
		sValue = oFilterXML.xml
	End If
	UpdateShippingEstimatorXML = sValue
End Function

Public Function AddDeliveryTypeNode(aArgs, sValue)
	sValue = sValue & "<DeliveryType>" & aArgs(1)("delivery_type") & "</DeliveryType>" & vbCrlf
	AddDeliveryTypeNode = sValue
End Function
%>
