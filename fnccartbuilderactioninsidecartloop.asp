<%
'**
 ' This function runs inside the cart loop in Cart Builder
 ' @since 10/6/2014
 ' @author jasonb
 ' @param aArgs(1) oRecordSet from CB 
 ' @param Val formatmoney value from session function
 ' @return getPrice
'**
Public Function fncCartBuilderActionInsideCartLoop(aArgs) %>
	<script>
		gaTrackCheckoutItems('<%=aArgs(1)("sc_sku")%>',<%=JSQ(aArgs(1)("p_nm"))%>,'','','<%=aArgs(1)("sc_price")%>','<%=aArgs(1)("sc_qty")%>')
	</script><%
End Function
%>