<%

'****************************************************************************
' fncSetPaymentPageTitle
'
' For PaymentActionBeforePageTitle
'
' [dcohen @ 2016-12-05] #103361
'****************************************************************************
Function fncSetPaymentPageTitle(ByRef aArgs)
		Dim rsOrder : Set rsOrder = aArgs(1)

		' update the page title
		REAL_PAGE_TITLE = Replace(REAL_PAGE_TITLE,"<=querystring=>","?sc_id=" & uSession.sc_id & "&username=" & uSession.CustomerUsername & "&form=checklogon&sha_id=" & request.querystring("sha_id"))

		' if it's a global address, hide the shipment information

		If False And CBool(Trim(rsOrder("s_global_address") & "")) Then
				%><style>#step3 {display: none;}</style><%
		End If

		fncSetPaymentPageTitle = True
End Function

Function fncAddShippingMessaging(ByRef aArgs)
		%>
		<div id="shipping-info-container" >
				<div class="shipping-info-msg" id="fedex-next-day" style="display:none;">
						<p><strong>FedEx Next Day</strong></p>
						<table class="table table-bordered table-condensed">
								<thead>
										<tr>
												<th>Order before 3:00pm EST Monday - Thrusday?</th>
												<th>Order after 3:00pm EST or Friday/Saturday/Sunday?</th>
										</tr>
								<tbody>
										<tr>
												<td>Arrives the next weekday.</td>
												<td>Ships Monday <br>
														Arrives Tuesday</td>
										</tr>
								</tbody>
						</table>
						<p>*No Saturday or Sunday deliveries</p>
				</div>
				<div class="shipping-info-msg" id="fedex-second-day" style="display:none;">
						<p><strong>FedEx Second Day</strong></p>
						<table class="table table-bordered table-condensed">
								<thead>
										<tr>
												<th>Order before 3:00pm EST Monday - Thrusday?</th>
												<th>Order after 3:00pm EST or Friday/Saturday/Sunday?</th>
										</tr>
								<tbody>
										<tr>
												<td>Arrives 2 business days after shipping.*</td>
												<td>Ships Monday <br>
														Arrives Wednesday</td>
										</tr>
								</tbody>
						</table>
						<p>*No Saturday or Sunday deliveries</p>
				</div>
		</div> <%
End Function

Function CartHasFreightProducts()
		Dim rsCart
		'check if freight is required'
		Set rsCart                           = New cDataAccessRecordSet3
		rsCart.SQL_ID_Minor_VersionNumber    = 0
		rsCart.SQL_ID_Revision_VersionNumber = 0
		rsCart.SQL_ID                        = "Base_Order_Table_Cart"
		rsCart.SearchString                  = "searchexact~o_id~" & uSession.SC_ID & "|AND|searchexact~p.freight_required~1"
		rsCart.ExtraFields                   = "p.freight_required"
		rsCart.AddDynamicJoin                "INNER JOIN products p on p.ws_id = sc.ws_id AND p.p_key = sc.p_id"
		Call rsCart.Open()

		If rsCart.EOF Then
				CartHasFreightProducts = False
		Else
				CartHasFreightProducts = True
		End If

		Set rsCart = Nothing
End Function

'****************************************************************************
' ALS returns all choices -- both freight and non-freight. We need to filter
' the results to show only freight or only non-freight as appropriate.
'
' Fail-over methods should only be returned by the ordering object if no other
' methods are returned, so we don't want to exclude those if they're there.
' (note: as of OOv420, it's always including fail-over methods. this is
' currently under investigation.)
'
' [dcohen @ 2016-12-12] #103361
'****************************************************************************
Function fncUpdateShipViaChoicesRS(ByRef aArgs, ByVal rsShipVias)
		If CartHasFreightProducts() Then
				rsShipVias.SearchString = rsShipVias.SearchString & "|AND|(searchleft~svc.sv_ref_id~3~LTL|OR|searchexact~sv.is_fail_over~1)"
		Else
				rsShipVias.SearchString = rsShipVias.SearchString & "|AND|(searchleftnot~svc.sv_ref_id~3~LTL|OR|searchexact~sv.is_fail_over~1)"
		End If

		Set fncUpdateShipViaChoicesRS = rsShipVias

End Function


'****************************************************************************
' fncDrawSvcJson
'
' show ship via choice data as JSON.
'
' [dcohen @ 2016-12-12] 103361
'****************************************************************************
Function fncDrawSvcJson(ByRef aArgs)
		Dim rsShipVias : Set rsShipVias = aArgs(1)
		Response.Write rsShipVias.BuildSafeJSON(True)

		fncDrawSvcJson = True
		Response.End
End Function

Function fncDrawLtlMessage(ByRef aArgs)
		%>
		<input type="hidden" name="opt3" id="opt3" value="0">
		<div id="ltl_info" class="hide">
				<div id="ltl_header" class="control-group">
						<div class="control-label">&nbsp;</div>
						<div class="controls"><b>Please provide name and phone number <br>for Delivery Notification:</b></div>
				</div>
				<div class="control-group">
						<div class="control-label">Name:</div>
						<div class="controls">
								<input type="text" id="opt1" class="formtextbox" name="opt1" data-msg="Please provide a name." style="width:200px">
						</div>
				</div>
				<div class="control-group">
						<div class="control-label">Phone:</div>
						<div class="controls">
								<input type="text" id="opt2" class="formtextbox" name="opt2" data-msg="Please provide a phone number." style="width:200px">
						</div>
				</div>
		</div>
		<%
End Function


'****************************************************************************
' fncDrawCustomOrderTotals
'
' custom order totals section. replaces existing logic in DrawOrderTotals.
'
' Customizations in this function:
' 1.) draw logo in "order totals" header
' 2.) display custom message if order is taxable (tax > 0)
' 3.) display custom markup for livechat
'
' [dcohen @ 2016-12-06] #103361 / cainb 2017-01-11 14:44:31 for New Splits & Payment(s)
'****************************************************************************
Function fncDrawCustomOrderTotals(ByRef aArgs)
		 %>
		<div id="totals_due_online" class="totals-online">
			<div class="total">
				<span class="left">Total Payment Due Online:</span>
				<span id="totals_due_online_value" class="right">$</span>
			</div>
		</div>
		<div id="totals_due_offline" class="totals-offline">
			<div class="total">
				<span class="left">Remaining Balance Due In Store:</span>
				<span id="totals_due_offline_value" class="right">$</span>
			</div>
			<div class="text">This is the amount due in store at time of pickup.</div>
		</div>
		<div id="totals_due_phone" class="totals-tbd">
			<div class="total">
				<span class="left">Balance Due By Phone:</span>
				<span id="totals_due_phone_value" class="right">TBD</span>
			</div>
			<div class="text">Please Call <strong>800-235-3325</strong> to pay this balance now.</div>
		</div>
		<div class="order-grandtotal-container order-total-dueonline">
			<div class="text">Total Payment Due Online:</div>
			<div class="total">
				<span class="left">Pay Now</span>
				<span id="totals_due_now_value" class="right">$</span>
			</div>
			<div class="form-actions">
				<button type="submit" id="submit_form_btn" class="btn btn-large btn-primary submit_form_btn" onclick="return validateCVV()">Place order now</button>
			</div>
		</div>
		<div class="payment-help">			
				<h4>Need Help?</h4>
				<small>Chat 8:00am - 6:00pm EST <br>
				Monday - Friday</small>
		</div>
		<%
		fncDrawCustomOrderTotals = True
End Function

'****************************************************************************
' fncFilterPayMethods
' Remove PayPal if we're shipping to a country other than the USA.
'
' [dcohen @ 2016-12-12]
'****************************************************************************
Function fncFilterPayMethods(ByRef aArgs, ByVal val)
		fncFilterPayMethods = val & "|AND|(searchexact~o.s_country~USA|OR|searchexactnot~pm.process_payment_using~paypal)|AND|searchexact~opmm.os_id~" & Request.QueryString("os_id")
End Function


'****************************************************************************
' fncDrawMessageBelowPaymentInfo
'
' custom message for pickup (global address) orders
' [dcohen @ 2016-12-14] #103361
'****************************************************************************
Function fncDrawMessageBelowPaymentInfo(ByRef aArgs)
		Dim rsOrder : Set rsOrder = aArgs(1)

		If CBool(Trim(rsOrder("s_global_address") & "")) Then

				%>
				<div class="control-group">
						<div class="control-label"></div>
						<div class="controls">
								<div class="alert alert-info">
										<i class="icon-info-sign"></i>
										Picking up today? Please allow 2 hours for us to prepare your order.<br>Payment will be collected at the time of pickup.
								</div>
						</div>
				</div>
				<%
		End If

		fncDrawMessageBelowPaymentInfo = True
End Function

%>
