<%
    '****************************************************************************
    '
    ' This is a copy of LIB Business Day Calculator v8800.2.3 with the class name
    ' changed from cBusinessDayCalculator.
    '
    ' It was put here to fix "name redefined" errors caused by including the
    ' library directly.
    '
    '****************************************************************************



    '**
     ' Calculates the expected ship date based on the lead-time passed in and the work days, holiday days, and cut-off time configured in expected_ship_date table
     '*
    Class cBusinessDayCalculator2

        '**
         ' @var date
         '*
        Dim m_dExpectedShipDate

        '**
         ' Lead time interval in days between init (order) and completion (shipped)
         ' @var int
         '*
        Dim m_iLeadTime

        '**
         ' The start date for the
         ' @var date
         '*
        Public StartDate

        '**
         '
         ' 0 = sunday, 1 = monday and so on. Expected value here is 1,2,3,4,5
         ' which means Monday, Tuesday, Wednesday, Thursday, Friday
         ' @var string
         '*
        Dim WorkDaysOfTheWeek

        '**
         ' Expected format is "7/31/2007,8/1/2007,8/3/2007"
         ' @var string
         '*
        Dim HolidayDates

        '**
         ' Expected format is "13:15", if hour value is >= 12, use PM in the UI, else use AM.
         ' @var string
         '*
        Dim CutOffTime

        '**
         ' Expected format is EST or PST.
         ' @var string
         '*
        Dim CutOffTimeZone

        '**
         ' String formatted as the current date (mm/dd/yyyy)
         ' @var string
         '*
        Dim Today

        '**
         ' String containing an error if the Expected Ship Date Data lookup fails
         ' @var string
         '*
        Private sConfigError


        '**
         ' Initialize the class with some defaults
         '*
        Public Sub Class_Initialize()

            'Set default vars
            m_iLeadTime     = 0
            'Today          = FormatDateTime(CDate("2013-11-05 15:33"),2)
            Today           = FormatDateTime(Now,2)
            StartDate       = Today
            CutOffTime      = "13:00"

            '*
             ' Set Ship Dates and check for an error
             '*
            sConfigError = configFromDbase()

        End Sub '-Public Sub Class_Initialize


        '**
         ' Calculates the expected ship date for a given number of expected days
         '
         ' @param iLeadTime int     number of days required to prep/ship a product
         '
         ' @returns date|string     returns the expected ship date or a string with an error message,
         '                          use IsDate() to determine the returned type.
         '*
        Public default Function ExpectedShipDate( iLeadTime )

            '*
             ' Set Lead time and check for an error
             '*
            sLeadTimeError = setLeadTime(iLeadTime)

            If VarType(sLeadTimeError) = 8 Then
                ExpectedShipDate = sLeadTimeError
                Exit Function
            End If '-If VarType(sError) = 8 Then

            '*
             ' Check for config error
             '*
            If VarType(sConfigError) = 8 Then
                ExpectedShipDate = sConfigError
                Exit Function
            End If '-If VarType(sError) = 8 Then

            '*
             ' Set the start date for the caluculation
             '*
            Call setStartDate()

            '*
             ' Calculate expected shipping date based on start date and ExpectedDays.
             '*
            m_dExpectedShipDate = CalculateShipDate()
            ExpectedShipDate    = m_dExpectedShipDate

        End Function 'Public Function ExpectedShipDate

        '**
         ' Calculates the expected ship date for a given product key
         '
         ' Uses provided product key to query the database and lookup the expected_date
         ' field on the product record.

         ' @param ProductKey string     char32 product key for the product to calculate for
         '
         ' @returns date|string     returns the expected ship date or a string with an error message,
         '                          use IsDate() to determine the returned type.
         '*
        Public Function ExpectedShipDateByProduct(ByVal ProductKey)

            'Verify char32 product key
            If Len(ProductKey) <> 32 Then
                ExpectedShipDateByProduct = "Invalid product key passed."
                Exit Function
            End If

            ' Get the expected days value for the product.
            Set oRsExpectedDays = new cDataAccessRecordSet3
            oRsExpectedDays.SQL_ID = "Product_Detail_No_DataSet"
            oRsExpectedDays.SQL_ID_Minor_VersionNumber = 0
            oRsExpectedDays.SQL_ID_Revision_VersionNumber = 0
            oRsExpectedDays.ExtraFields = "p.expected_days"
            oRsExpectedDays.SearchString = "searchexact~" & "p.p_key~" & ProductKey
            oRsExpectedDays.Open

            '*
             ' Set Expected Days and check for an error
             '*
            If Not oRsExpectedDays.EOF Then
                ExpectedShipDateByProduct = ExpectedShipDate( oRsExpectedDays("expected_days") & "" )

            Else
                ExpectedShipDateByProduct = "Invalid product key passed."

            End If '-If Not oRsExpectedDays.EOF Then

        End Function 'Public Function ExpectedShipDateByProduct


        '**
         ' Sets the ExpectedDays property
         '
         ' @param vLeadTime string|int (optional)       a string or integer representing the number
         '                                              of expected days. This is optional if the
         '                                              DEFAULT_EXPECTED_DAYS XML option is set.
         '
         ' @returns true|string     returns either a boolean true or an error string.
         '*
        Private Function setLeadTime( vLeadTime )

            '*
             ' If the expected days value is not set, default to XML option - DEFAULT_EXPECTED_DAYS.
             ' If XML option is not set, don't go ahead with the calculation.
             '*
            If vLeadTime <> "" Then
                m_iLeadTime = CInt( vLeadTime )
                setLeadTime = True
                Exit Function
            End If '-If vLeadTime <> "" Then

            If Trim(DEFAULT_EXPECTED_DAYS & "") <> "" Then
                m_iLeadTime = CInt( DEFAULT_EXPECTED_DAYS )
                setLeadTime = True
                Exit Function
            End If '-If Trim(DEFAULT_EXPECTED_DAYS & "") <> "" Then

            If m_iLeadTime = 0 Then
                setLeadTime = "Lead time days not specified for product."
                Exit Function
            End If

        End Function '-Private Function setLeadTime

        '**
         ' Get the expected ship date fields for the website.
         '
         ' @returns true|string     returns either a boolean true or an error string.
         '*
        Private Function configFromDbase()
            '- cainb - setting up possible use of multiple business day settings per site... (opsec2)
            '- Check User Settings on Session's XML (user_settings) for [esd_id]
            Dim sEsdKey
            Dim sSearch
            Dim sOrderBy

            sEsdKey = uSession.GetUserProfileValue("esd_id","")

            If Trim(sEsdKey&"") = "" Then
                sSearch = "(isnull~default_config~|OR|searchexact~default_config~1)"
                sOrderBy = "(CASE default_config WHEN 1 THEN 1 ELSE 2 END)"
            Else
                sSearch = "(searchexact~esd_key~" & sEsdKey & "|OR|searchexact~default_config~1)"
                sOrderBy = "(CASE esd_key WHEN '" & sEsdKey & "' THEN 1 ELSE 2 END)"
            End If

            Set oRsExpectedShipDate = new cDataAccessRecordSet3
            oRsExpectedShipDate.SQL_ID = "Expected_Shipping_Date"
            oRsExpectedShipDate.SQL_ID_Minor_VersionNumber = 0
            oRsExpectedShipDate.SQL_ID_Revision_VersionNumber = 0
            oRsExpectedShipDate.ExtraFields = "esd.ref_id, esd.daily_cutoff_time, esd.cutoff_time_zone, esd.work_days_of_week, esd.holiday_dates"
            oRsExpectedShipDate.SearchString = sSearch
            oRsExpectedShipDate.AddOrderBy sOrderBy, ""
            oRsExpectedShipDate.Open

            If Not oRsExpectedShipDate.EOF Then
                WorkDaysOfTheWeek   = oRsExpectedShipDate("work_days_of_week") & ""
                HolidayDates        = oRsExpectedShipDate("holiday_dates") & ""
                CutOffTime          = oRsExpectedShipDate("daily_cutoff_time") & ""
                CutOffTimeZone      = oRsExpectedShipDate("cutoff_time_zone") & ""

                configFromDbase = True

            Else
                HolidayDates=""
                configFromDbase = "No expected shipping date setup for website."

            End If '-If Not oRsExpectedShipDate.EOF Then

        End Function '-Private Function configFromDbase


        '**
         ' Returns a string of available ship dates based on the effective order.
         '
         ' @param LeadTime integer          Number of days AFTER effective order
         '                                  date to start.
         ' @param NumberOfDates integer     Number of valid dates to return.
         '
         ' @returns string      Returns a string of avaliable ship dates
         '                      deliminated by "|" character.
         '
         ' @author kbrown - #45313
         ' @since 2.29.2014
         '*
        Public Function getAvailableShipDates(LeadTime, NumberOfDates)

            sLastDate =  DateAdd("d", LeadTime, StartDate)
            sAvailableShipDates = sLastDate

            For i = 2 to NumberOfDates
                sLastDate = GetNextWorkingDay(sLastDate)
                sAvailableShipDates = sAvailableShipDates & "|" & sLastDate
            Next

            getAvailableShipDates = sAvailableShipDates

        End Function

        '**
         ' Determines what the start date should be for the Ship Date calculation
         '
         ' 1. Check if today is to be considered or not as the start date. Conditions are:
         '      a. cut off time has been exceeded (Defaults to 1:00 PM as cut off time).
         '      b. today is a holiday.
         '
         ' @returns (void)
         '*
        Public Sub setStartDate()

            '**
             ' Cut time hour (24 hour-time)
             ' @var string
             '*
            Dim CutTimeHr

            '**
             ' Current minutes (24 hour-time)
             ' @var string
             '*
            Dim CutTimeMin

            '**
             ' Current time hour (24 hour-time)
             ' @var string
             '*
            Dim CurrentHour
            CurrentHour = Hour(Now())

            '**
             ' Current time minutes (24 hour-time)
             ' @var string
             '*
            Dim CurrentMin
            CurrentMin  = Minute(Now())

            '*
             ' Check if today is a holiday, could be a non-wokring day or a holiday.
             '*
            If IsDateAHoliday(Today) Then
                StartDate = GetNextWorkingDay(Today)
                Exit Sub
            End If '-If IsDateAHoliday(Today) Then

            '*
             ' Check for a cut off time
             '*
            If Trim(CutOffTime&"")="" then
                CutTimeHr       = 13 '- 1pm
                CutTimeMin      = 0
            ElseIf Len(Trim(CutOffTime)) > 0 Then
                CutTimeHr       = Hour(CutOffTime)
                CutTimeMin      = Minute(CutOffTime)
            End If '-If Len(Trim(CutOffTime)) > 0 Then

            '*
             ' Check Hours and Minutes
             '*
            If CurrentHour >= CutTimeHr And CurrentMin >= CutTimeMin Then
                StartDate = GetNextWorkingDay(Today)
            End If '-If CurrentHour >= CutTimeHr And CurrentMin >= CutTimeMin Then

        End Sub '-Private Sub setStartDate

        '**
         ' Calculate expected shipping date based on start date and Lead Time.
         '
         ' @returns date    returns the calculated ship date formated as a
         '                  variant of subtype Date
         '*
        Private Function CalculateShipDate()
                Dim nIndex ' as Integer
                Dim sDate ' as Date

                nIndex = 0
                sDate = StartDate ' Start date already calculated.

                For nIndex = 1 To m_iLeadTime ' order will be shipped m_iLeadTime past start date.
                    sDate = GetNextWorkingDay(sDate)
                Next

                CalculateShipDate = sDate
        End Function '-Private Function CalculateShipDate

        '**
         ' Determines if the provided date falls on a weekend or holiday
         '
         ' @param sDate string      date to check
         '
         ' @returns bool    returns true if date falls on a weekend or holiday
         '                  and false otherwise.
         '*
        Private Function IsDateAHoliday(ByVal sDate) ' as Boolean
            Dim bRetval ' as Boolean
            Dim DayOfTheWeek ' as String.
            Dim Holiday 'as String

            DayOfTheWeek = Weekday(sDate, 0) & ""

            bRetval = False

            ' Check if the day of the week is not contained in the working days list.
            If InStr(1, WorkDaysOfTheWeek, DayOfTheWeek, 1) = 0 Then
                bRetval = True
            ElseIf Trim(HolidayDates&"")="" Then
                IsDateAHoliday = False
                Exit Function
            End If

            ' Loop through the holiday dates and compare the dates.
            If Not bRetval Then
                For Each Holiday In Split(HolidayDates,",",-1,1)
                    If IsDate(Holiday) Then
                        If CDate(Holiday) = CDate(sDate) Then
                            bRetval = True
                            Exit For
                        End If
                    End If
                Next
            End If

            IsDateAHoliday = bRetval
        End Function

        '**
         ' Recursive function to get the next working day.
         ' @param sDate string      string value representing a date
         '
         ' @returns date    returns an asp date object for the next working day
         '                  from the date provided (excludes weekends and holidays)
         '*
        Public Function GetNextWorkingDay(ByVal sDate)
            Dim NextDay

            NextDay = DateAdd("d", 1, CDate(sDate))

            If IsDateAHoliday(NextDay) Then
                sDate = DateAdd("d", 1, CDate(sDate))
                GetNextWorkingDay = GetNextWorkingDay(sDate)
            Else
                GetNextWorkingDay = NextDay
            End If
        End Function

    End Class '-Class cExpectedShipDate

%>
