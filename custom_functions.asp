<%
' ***************  READ ME  ********************
' ********************************************** 
' AFTER MAKING ANY CHANGES TO THIS FILE COMMIT CHANGES VIA SOURCE TREE 
' THE FOLLOWING WIKI DOC GOES OVER HOW TO USE SOURCE TREE 
' -- https://wiki.webstorepackage.com/index.php?title=Git_Workflow 
' Commit notes should include the task number and purpose of changes. 
' **********************************************
' ********************************************** 


Public Sub RegisterCustomSiteFunctions()

	add_filter "DataAccess3FilterGetPrice", "fncDataAccess3FilterGetPrice", 0
	add_filter "SessionsFilterFormatPrice", "fncSessionsFilterFormatPrice", 0

	add_action "CartBuilderColumnRemoveDrawColumnAction", "fncCartBuilderColumnRemoveDrawColumnAction", 0
	add_action "CartBuilderActionInsideCartLoop", "fncCartBuilderActionInsideCartLoop", 0
	add_filter "GoogleFeedPriceFilter", "FilterPrice", 1

	add_filter "ShippingEstimatorActionOrderMethod", "UpdateShippingMethod", 0
	add_action "ShippingEstimatorActionAfterEstimatorTable", "DrawFreightMessage", 0
	add_filter "ShippingEstimatorActionShipViaRecordset", "UpdateShipViaRecordset", 0
	add_filter "ShippingEstimatorFilterEstimatorXML", "UpdateShippingEstimatorXML", 0
	add_filter "ShippingEstimatorFilterEstimatorXMLInShippingMethod", "AddDeliveryTypeNode", 0

	'ATC changes'
	add_action "CartPostLogicActionUpdatePostData", "PostDefaultProdWarehouse", 0
	add_action "CartPostLogicActionTop", "AlterPostBackData", 0
	'add_action "ShowCartActionAfterBreadcrumbs", "DrawProductErr", 0

	'****************************************************************************
	' In NewCartUpgrade_BillShipCustomizations.asp:
	'****************************************************************************
		' local pickup
		add_action "BillShipReplaceActionDrawLocalPickupChoices", "fncDrawLocalPickupQuestion", 0
		add_action "BillShipActionBillingInfoPostLogic", "fncCustomOrderPostLogic", 0

		add_action "BillShipReplaceActionDrawFormOpenTag", "fncDrawCustomBillShipFormOpenTag", 0
		add_action "BillShipReplaceActionDrawAccount", "fncDrawCustomAccountInputs", 0
		add_filter "BillShipActionExtraFields", "fncUpdateBillShipExtraFields", 0
		add_action "BillShipActionAccountPostLogic", "fncUpdateAccountTaxMappings", 0
		add_action "BillShipActionJavaScriptDesktop", "fncDrawCustomBillShipJS", 0

		add_filter "BillShipFilterAccountNumber", "fncGenAccountNumber", 0
		add_filter "BillShipFilterAccountRefID", "fncGenAccountRefID", 0
		add_filter "BillShipActionShippingAddressPostLogic", "fncCustomAddressPostLogic", 0

		' password logic
		add_action "BillShipReplaceActionInvalidPasswordShowCartRedirect", "fncReturnTrue", 0
		add_action "BillShipReplaceActionDrawNewCustPwHiddenInput", "fncReturnTrue", 0
		add_filter "BillShipFilterNewCustomerDefaultEmailAddress", "fncSetDefaultEmailAddress", 0
		add_action "BillShipActionAboveSubmitButton", "fncDrawBillShipPasswordInputs", 0


	'****************************************************************************
	' in NewCartUpgrade_PaymentCustomizations.asp
	'****************************************************************************
		add_action "PaymentActionBeforePageTitle", "fncSetPaymentPageTitle", 0
		add_action "PaymentAddActionShippingBottom", "fncAddShippingMessaging", 0
		add_action "PaymentReplaceActionDrawOrderTotals", "fncDrawCustomOrderTotals", 0
		'add_action "PaymentAddActionDrawJSBottom", "fncDrawCustomPaymentJS", 0
		'add_filter "PaymentFilterShipViaChoicesRS", "fncUpdateShipViaChoicesRS", 0 '-cainb 2017-01-24 11:30:47 Turning off for multiple shipment/payments
		add_filter "PaymentAjaxGetShipViasDroplist", "fncDrawSvcJson", 0
		add_filter "PaymentAddActionShippingBottom", "fncDrawLtlMessage", 0
		add_filter "PaymentFunctionsFilterPayMethodsSearchString", "fncFilterPayMethods", 0
		add_action "PaymentAddActionPONumberAfter", "fncDrawMessageBelowPaymentInfo", 0


	' Show Cart: custom form
	add_action "ShowCartActionDrawExtraCheckoutForms", "fncDrawCustomCheckoutForm", 0

	' [avalis @ 6/22/2017] - Task #: 126741 - Removing, but client may want this in the future. Also removed the # from the include at the bottom: fncShowCartActionBelowCart.asp
	'add_action "ShowCartActionBelowCart", "fncShowCartActionBelowCart", 0

	add_action "ShowCartActionTotalsSection", "fncShowCartActionTotalsSection", 0

	'- cainb 2017-01-09 16:35:34 - Payment Page: Cart Display & Multiple Payments
	add_action "LibCartBuilderActionEndCartGroup", "fncLibCartBuilderActionEndCartGroup", 0
	' nschwartz - LibCartBuilder action for replacing name display
	add_action "LibCartBuilderActionCartItemName", "fncLibCartBuilderActionCartItemName", 0
	add_action "PaymentPageReplaceFormActions", "fncPaymentPageReplaceFormActions", 0
	
	'rmaas 2017-01-10 Showcart for above dev'
	add_action "ShowCartActionConfigureCartBuilder", "fncShowCartCartBuilderConfig", 0
	add_filter "LibCartBuilderFilterDescText", "fncLibCartBuilderFilterDescText", 0

	' dmecca 8.2.17 Show Cart display item pick options below cart item 
	add_action "LibCartBuilderActionAdditionalCartRow", "fncLibCartBuilderActionAdditionalCartRow", 0

	add_action "ShippingMethodAddEditActionBeforeFormEnd", "fncAddProdWarehouseSection", 0
	add_action "ShippingMethodAddEditActionPostLogicBeforeImport", "fncPostToProdWarehouseShipViaMap", 0

	add_action "ShowCartActionTopOfPage", "showCartCallBacks", 0

	add_action "CartPostLogicActionBeforeImportData", "fncCartPostLogicActionBeforeImportData", 0

	add_filter "CustomerAddEditFilterOrderHistoryDDTSearchString", "fncCustomerAddEditFilterOrderHistoryDDTSearchString", 0

	add_filter "ConfirmationFilterCartBuilderExtrafields", "fncAddTslPayMethodField", 0

	'CCTransactionManageHooks.asp
	add_action "CCTransactionBeforeActionLinks", "DrawTransactionColumns", 0

	' [cpalms @ 6/1/2017] - Don't draw the "EDIT" button for selected credit card on payment page
	add_action "LibSavedPaymentMethodsActionReplaceSelectedCCNButtons", "fncDrawSelectedCcnButtons", 0

	add_action "PaymentAddActionShippingBottom", "fncAddCallAheadInputs", 0

	' [avalis @ 6/22/2017] - Task #: 123200
	'****************************************************************************
	' Nextopia Products Feed Hooks
	'****************************************************************************
	add_filter "NextopiaRssFeedExtraFields", "fncAddNextopiaExtraFields", 0
	add_action "NextopiaProductFeedDrawXMLNodes", "fncAddOptional12", 0 


	'add_action "ShippingEstimatorActionShipViaRecordset", "RestrictShipVia", 0

	' Nextopia Category Updates - Webdriver Pages -  In file NextopiaCategoryUpdate.asp
	add_action "ProductCategoryAddEditActionDrawQuestions", "fncAddCategoryQuestions", 0 
	add_action "SearchTermsAddEditActionAboveOptionalSection", "fncSearchTermQuestions", 0 
	add_action "ProductSearchTermsManageDDT", "fncUpdateSearchTermsManageDDT", 0 
	add_action "ProductSearchTermAddEditSearchTermQuestion", "fncUpdateSearchTermsOnProductAddEdit", 0 
	add_action "ManageSearchGroupsDDT", "fncManageSearchGroupsDDT", 0 
	add_action "SearchGroupAddEditAboveOptFields", "fncAddHiddenFieldstoSearchGroupAddEdit", 0 
	add_action "ManageSearchTermsDetailoDDT", "fncUpdateSearchTermDetailManageDDT", 0
	add_action "SearchTermsAddEditSearchGroupQuestion", "fncUpdateSearchTermSearchGroupQuestion", 0 
	add_action "NextopiaProductFeedNodes", "fncUpdateNextopiaXmlNodes", 0
	add_action "NextopiaOverrideDataAccessRecordSet", "fncOverrideNextopiaDataAccess", 0

	' Unbound commerce updates ' In File UnboundProductFeedUpdates.asp
	add_filter "ProductFeedUnboundSearchStringFilter", "fncSplitUnboundProducts", 0
	
End Sub



Function fncCartPostLogicActionBeforeImportData(args)
	If (Trim(plCart.GetPostedValue("sku") & "") <> "") Then
		plData.ColumnValue("sku") = plCart.GetPostedValue("sku")
	End If
	'response.write plData.MaxRows()
	'plData.SetRow(0)
	'response.write plData.RemoveColumn("pw_id")
	'response.write plData.RenderData()
	'response.end
End Function

function showCartCallbacks(args)
	If Trim(Request.QueryString("prod_warehouse") & "") <> "" Then
		Response.clear
			Dim sWarehouseID
			Dim rsUpdate
			Dim sOdId
			Dim sSku : sSku = Trim(Request.QueryString("sku") & "")
		If Trim(Request.QueryString("updateStore") & "") <> "" Then
			If Trim(uSession.SessionField("pw_id") & "") <> "" Then
				sOldWhsId = uSession.SessionField("pw_id") 'Request.QueryString("old_wh")
			Else
				sOldWhsId = uSession.CustomerField("opt2")
			End If
			sWarehouseID = Request.QueryString("prod_warehouse")
			sOdId = Request.QueryString("od_id")
			Set rsUpdate = New cDataAccessRecordSet3
			rsUpdate.Execute("update sessions set pw_id=" & SQLQ(sWarehouseID) & " where s_key=" & SQLQ(uSession.SessionKey) & " and ws_id='AD7615E416A74059B38F722A2245EFD1'")
			If CBool(uSession.CustomerLoggedIn) Then
				rsUpdate.Execute("update customers set opt2=" & SQLQ(sWarehouseID) & " where c_key=" & SQLQ(uSession.CustomerKey) & " and ws_id='AD7615E416A74059B38F722A2245EFD1'")
			End If
			Response.Write "sWarehouseID: " & sWarehouseID & " - sOldWhsId: " & sOldWhsId 
			rsUpdate.Execute("update order_detail set pw_id=" & SQLQ(sWarehouseID) & " where o_id=" & SQLQ(uSession.SessionField("sc_id")) & " and od_key=" & SQLQ(sOdId) & " and ws_id='AD7615E416A74059B38F722A2245EFD1'")
			rsUpdate.Execute("update order_detail set pw_id=" & SQLQ(sWarehouseID) & " where o_id=" & SQLQ(uSession.SessionField("sc_id")) & " and pw_id in ('AF32C02B0D8245D89D306B730CEA1484','26E55F0CA931442A8BB6DA6BD3316807','E1CF16836F414D1DA5AB3C7E81C8D08F') and ws_id='AD7615E416A74059B38F722A2245EFD1'")
			Set rsUpdate = Nothing
		ElseIf Trim(Request.QueryString("od_id") & "") <> "" Then
			sWarehouseID = Request.QueryString("prod_warehouse")
			sOdId = Request.QueryString("od_id")
			Set rsUpdate = New cDataAccessRecordSet3
			If sSku <> "" Then
				rsUpdate.Execute("update order_detail set pw_id=" & SQLQ(sWarehouseID) & ", sku=" & SQLQ(sSku) & " where o_id=" & SQLQ(uSession.SessionField("sc_id")) & " and od_key=" & SQLQ(sOdId) & " and 	ws_id='AD7615E416A74059B38F722A2245EFD1'")
			Else
				rsUpdate.Execute("update order_detail set pw_id=" & SQLQ(sWarehouseID) & " where o_id=" & SQLQ(uSession.SessionField("sc_id")) & " and od_key=" & SQLQ(sOdId) & " and 	ws_id='AD7615E416A74059B38F722A2245EFD1'")
			End If
			Set rsUpdate = Nothing
		End If 
		Response.End
	End If
End Function 

Function fncCustomerAddEditFilterOrderHistoryDDTSearchString(args, sVal)
	Dim searchString
	searchString = sVal
	searchString = searchString & "|AND|(searchexactnot~o.multiple_ship_vias~1|OR|isnull~o.multiple_ship_vias)"
	fncCustomerAddEditFilterOrderHistoryDDTSearchString = searchString
End Function

Function fncAddCallAheadInputs(args) %>
	<div id="call-ahead-container">
		<label class="delivery-call-ahead ">
		Delivery Call Ahead Notification <br><small>Who should we call to confirm this delivery?</small>
		</label>
		<div class="formrow control-group">
			<div class="leftcol control-label">Name</div>
			<div class="rightcol controls">
				<input type="text" name="opt1" id="call-ahead-name" size="50" maxlength="40" data-title="Max Length: 40" data-placement="top" class="span4 tooltip-trigger ajax-save ajax-save-required"> 
				<span class="required-text">(required)</span>
			</div>
		</div>
		<div class="formrow control-group">
			<div class="leftcol control-label">Phone</div>
			<div class="rightcol controls">
				<input type="tel" name="opt2" id="call-ahead-phone" size="50" maxlength="50" value="" class="span4 mask-phone ajax-save ajax-save-required">
				<span class="required-text">(required)</span>
				<div class="help-block text-small phone-help">Format: (555-555-5555)</div>
			</div>
		</div>
	</div> 
	<script>
		$('.mask-phone').mask('999-999-9999');
	</script><%
End Function

' [avalis @ 7/13/2017] - Task #: 119594
Function RestrictShipVia(aArgs)
	Dim rsShipEstimate : Set rsShipEstimate = aArgs(1)
	rsShipEstimate.SearchString = rsShipEstimate.SearchString & "|AND|searchexact~sv.sv_key~92E29441547F458F967DA9428C51DC5B"
End Function

%>

<!--#include file="fncDataAccess3FilterGetPrice.asp"-->
<!--#include file="fncSessionsFilterFormatPrice.asp"-->
<!--#include file="fncCartBuilderColumnRemoveDrawColumnAction.asp"-->
<!--#include file="fncCartBuilderActionInsideCartLoop.asp"-->
<!--#include file="fncGoogleFeedFilterPrice.asp"-->
<!--#include file="fncShippingEstimatorHooks.asp"-->
<!--#include file="NewCartUpgrade_BillShipCustomizations.asp"-->
<!--#include file="fncDrawCustomCheckoutForm.asp"-->
<%' <!--include file="fncShowCartActionBelowCart.asp"--> %>
<!--#include file="fncShowCartActionTotalsSection.asp"-->
<!--#include file="NewCartUpgrade_PaymentCustomizations.asp"-->
<!--#include file="fncAddToCartPostChanges.asp"-->
<!--#include file="PaymentPageCartDisplayAndMultiplePayments.asp"-->
<!--#include file="ShowCartCartDisplay.asp"-->
<!--#include file="fncShippingMethodAddEditChanges.asp"-->
<!--#include file="CCTransactionManageHooks.asp"-->
<!--#include file="fncDrawSelectedCCNButtons.asp" -->
<!--#include file="NextopiaProductFeedHooks.asp" -->

<!--#include file="NextopiaCategoryUpdates.asp" -->
<!--#include file="UnboundProductFeedUpdates.asp" -->