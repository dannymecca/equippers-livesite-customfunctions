<%
'**
 ' This function overrides the decimal places returned
 ' for items ending with .00
 ' @since 7/24/2014
 ' @author jasonb
 ' @param aArgs(1) m_ors from Site Config 
 ' @param Val formatmoney value from session function
 ' @return getPrice
'**
Public Function fncSessionsFilterFormatPrice(aArgs, Val) 
	Dim FormatPrice : FormatPrice = Val
	
	If Right(FormatPrice,3) = ".00" then
		FormatPrice = Replace(FormatPrice,".00","")
	End If
	
	fncSessionsFilterFormatPrice = FormatPrice
End Function

%>