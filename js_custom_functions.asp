<!-- This is a syntax hack to make everything below syntax highlight as javascript despite the file
extension being ".asp". This works because everything here and above is cleared by Response.Clear -->
<script><%
' Clear the above text and set the content type as javascript
Response.Clear
Response.ContentType = "application/javascript" %>

registerHook({
	name: 'shipping_estimator_child_nodes_before_table',
	pages: 'shipping_estimator.asp',
	func: function (oArgs) {
		var freight = oArgs.ref_id;
		var cell3 	= document.createElement("TD");
		if (freight.substr(0,3) == "LTL") {
			$('.freight_msg').show();
		} else {
			$('.freight_msg').hide();
		}
	}
});

registerHook({
	name: 'shipping_estimator_child_nodes_show_zero_price_methods',
	pages: 'shipping_estimator.asp',
	func: function (oArgs) {
		oArgs.cell3.innerHTML = "&nbsp;";
		oArgs.row.appendChild(oArgs.cell3);
	}
});

registerHook({
	name: 'shipping_estimator_child_nodes_show_zero_price_methods_nonzero_price',
	pages: 'shipping_estimator.asp',
	func: function (oArgs) {
		var orig_price = GetNodeValue(oArgs.element, "OriginalPrice", 0)
		console.log(orig_price);

		var discount = orig_price - oArgs.price;
		discount = formatCurrency(discount);

		if(discount > 0){
			oArgs.cell3.appendChild(document.createTextNode('$' + orig_price +' Less Free Shipping: ($' + discount +')'));
		}
		else{
			oArgs.cell3.innerHTML = "&nbsp;";
		}

		oArgs.cell3.style.paddingLeft = "10px";
		oArgs.cell3.className = "se-discounttext";

		oArgs.row.appendChild(oArgs.cell3);
	}
});

/**
 * password validation on bill-ship page
 * [dcohen @ 2016-11-21] #103359
 */

const VALID_PASSWORD_REGEX = /^[\w@\-\.]{6,}$/i;
registerHook({
	name: 'bill_ship_custom_form_validation',
	pages: 'account.asp',
	func: function(oArgs, return_values){
		// debugger;
		var $ = jQuery,
			frm = oArgs.frm,
			bPass = oArgs.pass;

		if($("#nusername").length){ // only run this check if we're creating a new user
			var sPW = $("#pw", frm).val();
			if(bPass &&  sPW !== $("#pw2", frm).val()){ // do passwords match?

				bPass = false;
				$("#pw", frm).focus();
				alert('Passwords do not match.');
			} else if(!VALID_PASSWORD_REGEX.test(sPW)){ // is the entered password valid?
				bPass = false;
				$("#pw", frm).focus();
				alert('Password not valid! This password does not comply with character restrictions. It must be a minimum of 6 characters and can only contain letters, numbers, and the following symbols: dash, period, @, and underscore.');
			}
		}

		return_values['pass'] = bPass;
		return return_values;
	}
});


/**
 * show special message if fedex next day or fedex second day
 * (see fncAddShippingMessaging)
 */
registerHook({
	name: 'payment_ship_via_changed',
	pages: 'payment.asp',
	func: function(args, return_values){
		var $ = jQuery;
		var sChoiceKey = $("#svc_key").val();
		if(sChoiceKey.length || typeof ofConfig.aShipViaChoices === 'undefined'){
			return false;
		}

		var oChoice = ofConfig.aShipViaChoices.filter(function(choice){ return (choice.svc_key === sChoiceKey); })[0];
		var $container = $("#ltl_info");

		if(oChoice.sv_ref_id.substring(0,3) === 'LTL' ){
			// show and require additional LTL inputs
			$container.removeClass('hide');
			$("input#opt1, input#opt2", $container).rules('add','required');
		} else {
			// ensure LTL inputs are hidden and not required.
			$("#ltl_info").addClass('hide');
			$("input#opt1, input#opt2", $container).rules('remove','required');


			// non-LTL. Check if it's fedex next/second and show custom message if so.
			var svc_nm = oChoice.nm;
			$('div.shipping-info-msg', '#shipping-info-container').hide();

			var match = svc_nm.match(/^fedex (next|second) day/i);

			if(match && typeof match[0] === 'string'){
				 var sMessageID = match[0].toLowerCase().replace(/\s/g,'-');
				 $('div.shipping-info-msg#' + sMessageID, '#shipping-info-container').show()
			}
		}
	}
});

