<%
Function fncDrawSelectedCcnButtons(ByRef aArgs)
	'	aArgs(1) -> oSelectedCreditCard
	'	aArgs(2) -> edit button label
	'	aArgs(3) -> remove button label
	'	aArgs(4) -> change button label
	'	aArgs(5) -> add button label
	Dim sRemoveCardLabel : sRemoveCardLabel = aArgs(3)
	%>
	<div class="btn-group">
		<button type="button" id="ccpm_remove_card" class="btn btn-mini"><%= sRemoveCardLabel %></button>
	</div>
	<%
End Function
%>