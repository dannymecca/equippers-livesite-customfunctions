<%
'**
 '
'**
Public Function fncShowCartActionBelowCart(aArgs)
	Dim rsAlsoBought
	Set rsAlsoBought = new cDataAccessRecordSet3
	rsAlsoBought.SQL_ID = "Base_Order_Detail"
	rsAlsoBought.SQL_ID_Minor_VersionNumber = 0
	rsAlsoBought.SQL_ID_Revision_VersionNumber = 0
	rsAlsoBought.searchString = "searchexact~o_id~" & uSession.SessionField("sc_id") & "|AND|searchinnot~pof.opt1~Base_Order_Detail.0.0~searchexact^o_id^" _
							  & uSession.SessionField("sc_id") & "|AND|searchexactnot~pof.opt1~"
	rsAlsoBought.AddDynamicJoin "left join products p on od.p_id=p.p_key"
	rsAlsoBought.AddDynamicJoin "left join products_opt_fields pof on pof.p_key=p.p_key"
	rsAlsoBought.ExtraFields = "pof.opt1, p.p_key"
	rsAlsoBought.Open

	dim sAlsoBoughtSearch
	sAlsoBoughtSearch = ""
	Do Until rsAlsoBought.EOF
		aProdAlsoBought = Split(rsAlsoBought("opt1"),";")
		for each item in aProdAlsoBought
				sAlsoBoughtSearch = sAlsoBoughtSearch + "~" + item
		Next
		Call rsAlsoBought.MoveNext()
	Loop

	rsAlsoBought.Close
	Set rsAlsoBought = Nothing

	If sAlsoBoughtSearch <> "" Then

		Dim rsAddOnProd
		Set rsAddOnProd 							= new cDataAccessRecordSet3
		rsAddOnProd.SQL_ID 							= "Manage_Products_With_Datasets_New"
		rsAddOnProd.SQL_ID_Minor_VersionNumber 		= 0
		rsAddOnProd.SQL_ID_Revision_VersionNumber 	= 1
		rsAddonProd.UseDistinctQuery 				= True
		rsAddOnProd.ExtraFields 					= "Top 5 IsNull(p.include_in_search_results,1) as include_in_search_results, p.uom_sales_conversion," _
													& "IsNull(p.sretail_price, 0) as sretail_price, IsNull(p.retail_price, 0) as retail_price, p.sku, p.ds, p.pic,"_
													& "p.thumb, p.lg_pic, p.orderby, p.opt1, p.opt2, p.opt3, p.opt4, p.opt5, p.opt6, p.imageurl, p.searchfield1, p.searchfield2, p.searchfield3, p.searchfield4, p.searchfield5, p.searchfield6, p.searchfield7, p.searchfield8, p.searchfield9, p.searchfield10, p.searchfield11, p.searchfield12, p.searchfield13, p.searchfield14, p.searchfield15, IsNull(p.show_add_cart, 1) as show_add_cart, IsNull(p.alt_pic_t, p.nm) as alt_pic_t, p.parent_child_type, IsNull(p.use_cart_options, 0) as use_cart_options, p.cart_question, p.cart_options, IsNull(p.show_prod_price, 1) as show_prod_price, IsNull(p.case_qty,0) case_qty, p.price_display_type, IsNull(p.min_order_qty, 0) As min_order_qty, IsNull(p.max_order_qty, 0) As max_order_qty, IsNull(p.optional8, '') As opt8, p.child_retail_price,IsNULL(p.flag1,0) as flag1,IsNULL(p.flag2,0) as flag2,IsNULL(p.flag3,0) as flag3,IsNULL(p.flag4,0) as flag4, p.nm, IsNull(p.nm, '') As nm, p.optional7 opt7, p.optional8 opt8, p.optional9 opt9, p.optional10 opt10, p.optional11 opt11, p.optional12 opt12, p.optional13 opt13, p.optional14 opt14, p.optional15 opt15,IsNull(p.orderby,0), p.retail_price, staticpages.se_directory, staticpages.se_pagename, staticpages.se_directory + '/' + staticpages.se_pagename as se_link, CAST(IsNull(p.show_add_cart,1) AS VARCHAR(1)) AS show_add_cart , CAST(IsNull(p.show_prod_price,1) AS VARCHAR(1)) AS show_prod_price ,product_class,cost,wholesale,wholesale2,wholesale3,wholesale4,cat_id,product_class_b,product_class_c,product_class_d,product_class_e,wholesale5,wholesale6,wholesale7,wholesale8,wholesale9,wholesale10,is_kit_container,use_kit_content_prices,apply_price_adjustment,CASE WHEN IsNull(p.inv_item, 1) = 0 THEN 'noninv' WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN (IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END AS calc_inv_status ,CASE CASE WHEN IsNull(p.inv_item, 1) = 0 THEN 'noninv' WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN (IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END WHEN 'noninv' THEN '' WHEN 'in' THEN CASE WHEN IsNull(p.idp_instock_msg, '') <> '' THEN p.idp_instock_msg ELSE idp.in_stock_msg END WHEN 'onorder' THEN CASE WHEN IsNull(p.idp_onorder_msg, '') <> '' THEN p.idp_onorder_msg ELSE idp.on_order_msg END ELSE CASE WHEN IsNull(p.idp_outstock_msg, '') <> '' THEN p.idp_outstock_msg ELSE idp.out_of_stock_msg END END AS calc_inv_message ,CASE WHEN IsNull(p.inv_item, 1 ) = 0 THEN '0' WHEN CASE WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN (IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END = 'in' AND IsNull(idp.in_stock_show_qty, 0) <> 0 THEN CASE WHEN IsNull(idp.use_qty_great_than, '0') = '1' AND IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) >= IsNull(idp.qty_great_than_value, 0) THEN idp.qty_great_than_display ELSE CASE WHEN IsNull(idp.use_qty_less_than, '0') = '1' AND IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) <= IsNull(idp.qty_less_than_value, 0) THEN IsNull(idp.qty_less_than_display, '0') WHEN IsNull(idp.use_qty_less_than, 0) = 1 AND IsNull(idp.qty_less_than_value, '0') BETWEEN IsNull(p.qty_available, 0) AND IsNull(p.qty_buffer, 0) THEN IsNull(idp.qty_less_than_display, '0') ELSE Cast(IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) AS VARCHAR(15)) END END ELSE CAST(IsNull(p.qty_available, 0) as VARCHAR) END AS calc_inv_qty ,CASE CASE WHEN IsNull(p.inv_item, 1) = 0 THEN 'noninv' WHEN IsNull(idp.status_override, '2') = '2' THEN 'in' WHEN (IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0)) > 0 THEN 'in' WHEN IsNull(p.qty_available, 0) - IsNull(p.qty_buffer, 0) + IsNull(p.qty_on_po, 0) > 0 THEN 'onorder' ELSE 'out' END WHEN 'noninv' THEN '1' WHEN 'in' THEN CASE WHEN IsNull(idp.in_stock_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'onorder' THEN CASE WHEN IsNull(idp.on_order_show_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END WHEN 'out' THEN CASE WHEN IsNull(idp.out_of_stock_add_to_cart, 1) <> 0 THEN '1' ELSE '0' END END AS calc_inv_show_cart ,isnull(out_of_stock_show_qty, '0') AS out_of_stock_show_qty ,isnull(in_stock_show_qty, '0') AS in_stock_show_qty ,IsNull(in_stock_show_msg, '0') AS in_stock_use_msg ,IsNull(out_of_stock_show_msg, '0') AS out_of_stock_use_msg ,IsNull(on_order_show_msg, '0') AS on_order_show_msg ,IsNull(p.inv_item, 1) as inv_item, s.sc_id as sc_key, (SELECT distinct count(sales_order_number) FROM external_order_sales_order_detail eo where eo.sku=p.sku) as order_count"
		rsAddOnProd.searchString 					= "searchlist~p.p_key" & sAlsoBoughtSearch & "|AND|searchexact~p.status~1|AND|searchgreaterthan~p.qty_available~0|AND|searchinnot~p.p_key~Products_On_Order.0.1~searchexact^o_id^" & uSession.SessionField("sc_id")
		rsAddOnProd.AddDynamicJoin 					"left join static_pages staticpages on staticpages.content_id= (CASE WHEN p.parent_child_type='child' THEN  p.parent_p_id ELSE p.p_key END)"
		rsAddOnProd.AddDynamicJoin 					"Left Join inventory_display_profiles idp On idp_key = p.idp_id "
		rsAddOnProd.AddDynamicJoin 					"Left Join sessions s On s_key = '" + uSession.customerkey +"'"
		rsAddonProd.AddOrderBy 						"order_count", "desc"
		rsAddOnProd.Open
		%><div class="also-bought">
			<div class="items">
				<div class="items-row">
					<div id="ab-title">
						<div id="ab-text"><h4>Customers with these items also purchased:</h4></div>
						<div id="ab-close"><strong><a onClick="$('.also-bought').hide();">NO THANKS</a></strong></div>
					</div><%
					iCount = 0
					Do While Not rsAddOnProd.EOF

						iCount = iCount + 1
						If iCount = 5 Then
							iCount = 0 %>
							</div>
							<div class="items-row">
								<%
						End If

						bFoundOne = True

						If True Then %>
						<div class="item-container">
							<form id="atc_recommended_products_<%=rsAddOnProd("p_key")%>" action="<%=ORDERFRONTURL%>/i_i_add_to_cart.asp?type=repeat" method="POST">
							<%
							End If
							%>
							<div class="thumb">

								<%
								If Trim(rsAddOnProd("se_link") & "") <> "" Then
									sProdLink = STOREFRONTURL & "/" & rsAddOnProd("se_link")
								Else
									sProdLink = STOREFRONTURL & "/pc_product_detail.asp?key=" & rsAddOnProd("p_key")
								End If

								If Trim(rsAddOnProd("thumb") & "") <> "" Then
								%>

									<a href="<%=sProdLink%>" title="<%=Server.HTMLEncode(rsAddOnProd("nm"))%>" onClick="gaTrackClick('<%=rsAddOnProd("sku")%>','<%=Replace(Replace(rsAddOnProd("nm"),"""","\x22"),"'","\'")%>','<%=sCatName%>','<%=rsAddOnProd("searchfield6")%>','Add-On','<%=iCount%>');">
										<img src="images/<%=rsAddOnProd("thumb")%>" alt="<%=Server.HTMLEncode(rsAddOnProd("nm"))%>">
									</a>

								<%
								End If '- If Trim(rsAddOnProd("thumb") & "") <> "" Then

								If len(trim(rsAddOnProd("nm"))) > 36 Then
									sAccessoryNM = "..."
								Else
									sAccessoryNM = ""
								End If
								%>

							</div>
							<div class="item_row nm"><strong><a href="<%=sProdLink%>" title="<%=Server.HTMLEncode(rsAddOnProd("nm"))%>" onClick="gaTrackClick('<%=rsAddOnProd("sku")%>','<%=Replace(Replace(rsAddOnProd("nm"),"""","\x22"),"'","\'")%>','<%=sCatName%>','<%=rsAddOnProd("searchfield6")%>','Add-On','<%=iCount%>');"><%=rsAddOnProd("nm")%></a></strong></div>
							<% If LCase(rsAddOnProd("parent_child_type")) <> "parent" Then %>
							<div class="item_row sku">SKU: <%=rsAddOnProd("sku")%></div>

							<div class="item_row price"><%
									If rsAddOnProd("opt8") <> "" Then
										sRelatedUnit = " " & rsAddOnProd("opt8")
									End If %>
									<div class="retailprice"><span class="price"><%=uSession.FormatMoney(rsAddOnProd("retail_price")) & "</span><span class='unit'>" & sRelatedUnit %></span></div><%

									If rsAddOnProd("sretail_price") & "" <> "" And CBool(SHOW_PRICE_COMPARE) Then
										If cDbl(rsAddOnProd("sretail_price")) > rsAddOnProd("retail_price") Then %>
											<div class="listprice"><%=uSession.FormatMoney(rsAddOnProd("sretail_price"))%></div><%
										End If '- If cDbl(rsAddOnProd("sretail_price")) > rsAddOnProd.GetPrice() Then

									End If '- If rsAddOnProd("sretail_price") & "" <> "" And CBool(SHOW_PRICE_COMPARE) Then %>
								</div><!-- item_row --><%
								If CBool(bShowLoginMessage) And Not CBool(bShowPrice) And REQUIRE_LOGIN_HELPTIP <> "" And CBool(SHOW_PRICE_DISPLAY_TYPE) Then %>
									<div class="product_require_login">
										<%=Replace(REQUIRE_LOGIN_HELPTIP, "<returnpage>", sCurrentPage)%>
									</div><%
								End If

								If sPriceDisplayType = "require_login_or_atc" And Not CBool(bShowPrice) And REQUIRE_ATC_HELPTIP <> "" And CBool(SHOW_PRICE_DISPLAY_TYPE) Then %>
									<div class="item_help">
										<%=REQUIRE_ATC_HELPTIP%>
									</div><%
								End If %>

							<% End If
							iCaseQty = 1
							If Cint(rsAddOnProd("case_qty")) > 1 Then
								iCaseQty = Cint(rsAddOnProd("case_qty")) %>
								<p id="case_qty_msg"><%
									Response.Write Replace(CASE_QTY_TEXT,"<qty>", iCaseQty) %>
								</p>
								<div class="atc-container"><%
							Else %>
								<div class="atc-container no-case-qty"> <%
							End If
							If True Then
								%><div class="qty-label"><label for="qty_<%=rsAddOnProd("p_key")%>">QTY</label></div><%
								If Trim(rsAddOnProd("sc_key") & "") <> "" Then %>
									<input type="number" id='qty_<%=rsAddOnProd("p_key")%>' onclick="this.focus();this.select();" onkeypress="return parent.NumbersOnly(this, event);" maxlength="9" class="qtyinput" min='0' step='<%=iCaseQty%>' value="" disabled="disabled" />
								<% Else %>
									<input type="number" name='qty_<%=rsAddOnProd("p_key")%>' onclick="this.focus();this.select();" onkeypress="return parent.NumbersOnly(this, event);" maxlength="9" class="qtyinput" min='0' step='<%=iCaseQty%>' value="<%=iCaseQty%>" />
								<% End If
							Else %>
								<input type="hidden" name="qty_<%=rsAddOnProd("p_key")%>" onclick="this.focus();this.select();" onkeypress="return parent.NumbersOnly(this, event);" maxlength="9" class="qtyinput" value="1" />
							<% End If %>
							<input type="hidden" id="minqty_<%=rsAddOnProd("p_key")%>" class="qtyinput" value="<%=rsAddOnProd("min_order_qty")%>" />
							<input type="hidden" id="case_qty_<%=rsAddOnProd("p_key")%>" class="qtyinput" value="<%=rsAddOnProd("case_qty")%>" />
							<input type="hidden" id="price_<%=rsAddOnProd("p_key")%>" class="qtyinput" value="<%=rsAddOnProd("retail_price")%>" />
							<input type="hidden" name="keys" class="qtyinput" value="<%=rsAddOnProd("p_key")%>" /><%

							If ( ( sPriceDisplayType <> "require_login_for_price_and_atc" And sPriceDisplayType <> "hide" ) Or ( sPriceDisplayType = "require_login_for_price_and_atc" Or sPriceDisplayType = "hide" And sInvDispProfKey = "hide_add_to_cart_and_price" ) ) Then
								Response.Write rsAddOnProd.InventoryMessage
							End If

							If False Then
							%>

							<div class="item_row caseqty">Case Qty: <%=rsAddOnProd("case_qty")%></div>

							<%
							End If '- If CBool(USE_CASE_QTY) Then

							If True And LCase(rsAddOnProd("parent_child_type")) <> "parent" Then

								If Trim(rsAddOnProd("sc_key") & "") <> "" Then
									%><button type="submit" disabled="disabled" class="btn btn-addtocart btn-disabled"><%=ADDED_TO_CART_BUTTON_TEXT%></button><%
								Else
									%><button type="submit" class="btn btn-addtocart">ADD TO CART</button><%
								End If %>
								</div>
								</form><%
							Else
								'jb - show select options text for parents %>
								<div id="atc-no-price" class="noprice"><a href="<%=sProdLink%>" title="<%=rsAddOnProd("nm")%>"><%=OPTIONS_TEXT%></a></div><%
							End If %>

						</div><%

						Call rsAddOnProd.MoveNext()
					Loop '- Do While Not rsAddOnProd.EOF %>
				</div>
			</div>
		</div><%
		rsAddOnProd.Close
		Set rsAddOnProd = Nothing
	End If '-If sAlsoBoughtSearch <> "" Then
End Function
%>
