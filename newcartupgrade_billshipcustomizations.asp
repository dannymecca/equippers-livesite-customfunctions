<!-- #include file = "../../../../../../Components/Base Line Component/Component Files/Standard Library/LIB_Equippers_Functions_10000-0-9_90F87DC0C0.asp" -->

<%
' previous version of LIB Business Day Calculator
' <!-- include file = "../../../../../../Components/base line component/component files/standard library/LIB_Business_Day_Calculator_8800-2-0_A926540198.asp"  id = "11C08C3C361A420F974355B49A4EBBA1"  id2 = "A9265401981049648855C6F8BA425973" -->
' <!-- include file = "../../../../../../Components/Base Line Component/Component Files/Standard Library/LIB_Business_Day_Calculator_8800-2-3_3FEE56B777.asp" -->
%>
<!-- #include file = "BusinessDayCalculator.asp" -->

<%
'****************************************************************************
' fncDrawLocalPickupQuestion
' Draw local pickup question with custom logic + pickup date
'
' For hook BillShipReplaceActionDrawLocalPickupChoices.
'
' [dcohen @ 2016-11-22] #103359
'****************************************************************************
Function fncDrawLocalPickupQuestion(ByRef aArgs)
    Dim rsBillShipInfo : Set rsBillShipInfo = aArgs(1)
    Dim sAccountID : sAccountID = aArgs(2)
    Dim sDefaultKey : sDefaultKey = aArgs(3)

    'Call drawCustomPickupQuestion(rsBillShipInfo, sAccountID, sDefaultKey)

    '- cainb 2017-01-25 10:48:13 - Needed for post logic
    Response.Write "<input type=""hidden"" name=""is_exempt"" value=""" & rsBillShipInfo("tax_exempt") & """>"
    
    ' expected pickup date
    Call subDrawPickupDateQuestion()

    fncDrawLocalPickupQuestion = True
End Function

Sub drawCustomPickupQuestion(rsBillShipInfo, sAccountID, sDefaultKey)
    Dim rsGlobalAddresses
    Dim iRowNumber

    Dim sSearchString

    sSearchString = "searchexact~sha.globaladdress~@1"

    ' show ALL non-exempt addresses PLUS exempt addresses from states where the account is marked
    ' as exempt.

    ' The logic to show/hide which addresses get displayed is in BillShipCustomLogic.js
    If CBool(rsBillShipInfo("tax_exempt") & "") Then
        sSearchString = sSearchString & "|AND|(searchexact~tax_exempt~0|OR|searchlist~s_state~" & Replace(Trim(rsBillShipInfo("exempt_states") & ""), ",", "~") & ")"
    Else
        sSearchString = sSearchString & "|AND|searchexact~tax_exempt~0"
    End If

    %>
    <div class="formrow control-group">
        <div class="leftcol control-label"></div>
        <div class="rightcol controls">

        <table cellpadding="0" cellspacing="0" border="0" class="input-table radios">
        <tbody>
        <%
            Set rsGlobalAddresses                     = New cDataAccessRecordSet3
            rsGlobalAddresses.SQL_ID                        = "All_Customer_Shipping_Addresses"
            rsGlobalAddresses.SQL_ID_Minor_VersionNumber    = 0
            rsGlobalAddresses.SQL_ID_Revision_VersionNumber = 5
            rsGlobalAddresses.SearchString                  = sSearchString ' "searchexact~sha.globaladdress~@1"
            rsGlobalAddresses.ExtraFields                   = "IsNull(s_add1, '') + ', ' + IsNull(s_zip, '') + (Case When IsNull(sha.nm, '') <> '' Then ' (' + sha.nm + ')' Else '' End) as DisplayField, sha.s_state, sha.ref_id, sha.tax_exempt, sha.sacct_code"
            Call rsGlobalAddresses.AddOrderBy("s_add1", "ASC")
            Call rsGlobalAddresses.AddOrderBy("tax_exempt", "DESC")
            Call rsGlobalAddresses.Open()

            iRowNumber = 0
            Do Until rsGlobalAddresses.EOF
                %>
                <tr><td>
                <label for="sha_key_<%=iRowNumber%>" class="radio">
                    <input
                        type="radio"
                        name="sha_key"
                        id="sha_key_<%=iRowNumber%>"
                        value="<%=rsGlobalAddresses("sha_key")%>"
                        data-ref_id="<%=rsGlobalAddresses("ref_id")%>"
                        data-s_state="<%=rsGlobalAddresses("s_state")%>"
                        data-tax_exempt="<%=LCase(CBool(rsGlobalAddresses("tax_exempt") & ""))%>"
                        data-sacct_code="<%=rsGlobalAddresses("sacct_code")%>"
                         />
                        <% ' onchange="getCartAddress();" %>
                    <%=rsGlobalAddresses("DisplayField")%>
                </label>
                </td></tr>
                <%
                iRowNumber = iRowNumber + 1
                Call rsGlobalAddresses.MoveNext()
            Loop

            Set rsGlobalAddresses = Nothing %>
        </tbody>
        </table>
        </div> <!-- .rightcol.controls -->
    </div> <!-- .formrow.control-group -->
    <%
End Sub

'****************************************************************************
' subDrawPickupDateQuestion
' Called by fncDrawLocalPickupQuestion.
'
' Custom logic: Business Day Calculator / Expected Pickup Date
' [dcohen @ 2016-11-21] #103359
'****************************************************************************
Sub subDrawPickupDateQuestion()
    Dim oBusinessDayCalculator, _
        vCart, _
        dEffective_Order_Date, _
        iMaxLeadTime, _
        sFirstDate, _
        sFirstPulldownItemText, _
        iLeadTime, _
        sOtherDates

    ' previously XML options in Bill-Ship - Equippers
    Dim SHOW_PICKUP_DATE, _
        PICKUP_DATE_TITLE, _
        PICKUP_DATE_LABEL, _
        PICKUP_DATE_HELP_TEXT, _
        PICKUP_DATE_SUFFFIX_FIRSTDATE, _
        PICKUP_DATE_SUFFIX_OTHERDATES, _
        EXPECTED_PICKUP_LEADTIME_SOURCE, _
        EXPECTED_PICKUP_FIRST_ELIGIBLE_DATE_LEADTIME, _
        EXPECTED_PICKUP_QTY_OF_DAYS_ELIGIBLE

    SHOW_PICKUP_DATE = "0"
    PICKUP_DATE_TITLE = "Choose a Pickup Date"
    PICKUP_DATE_LABEL = "Pickup Date:"
    PICKUP_DATE_HELP_TEXT = "Local Pickup Date."
    PICKUP_DATE_SUFFFIX_FIRSTDATE = "after 10 AM"
    PICKUP_DATE_SUFFIX_OTHERDATES = "8 AM to 5 PM"
    EXPECTED_PICKUP_LEADTIME_SOURCE = "1"
    EXPECTED_PICKUP_FIRST_ELIGIBLE_DATE_LEADTIME = "1"
    EXPECTED_PICKUP_QTY_OF_DAYS_ELIGIBLE = "10"



    '- Set Class object
    Set oBusinessDayCalculator = New cBusinessDayCalculator2
    '- This method will set all the required parameteres based on Expected_Ship_Date table settings.
    oBusinessDayCalculator.Class_Initialize
    '- This method wil calculate the Start Date based on Expected_Ship_Date table settings.
    oBusinessDayCalculator.setStartDate
    '- Set Effective order date
    dEffective_Order_Date =oBusinessDayCalculator.StartDate

    Set vCart = New cDataAccessRecordSet3
    vCart.SQL_ID = "Base_Order_Table_Cart"
    vCart.SQL_ID_Minor_VersionNumber = 0
    vCart.SQL_ID_Revision_VersionNumber = 1
    vCart.AddDynamicJoin ( "Left join products p on p.p_key = sc.p_id" )
    vCart.AddDynamicJoin ( "Left join product_lead_times plt on p.plt_id = plt.plt_key" )
    vCart.SearchString = "searchexact~sc.o_id~" & uSession.SC_ID
    vCart.ExtraFields = "(case when p.expected_days is null then isnull(plt.lead_time,7) else p.expected_days end) as calculated_lead_time"
    vCart.Open

    If vCart.EOF Then
        Exit Sub
    End If 'If Not vCart.EOF Then

    ' need to change this to use the order_detail table instead of shopping_cart
    '


    'Response.Write "<fieldset class=""formblock"" id=""pickup-date"">" & vbCrlf
    Response.Write "<p class=""formblock-heading"">" & PICKUP_DATE_TITLE & "</p>" & vbCrlf
    Response.Write "<div class=""formrow"">"
    Response.Write "<span class=""leftcol"">" & PICKUP_DATE_LABEL
    'Call drawHelpTip(PICKUP_DATE_TITLE,PICKUP_DATE_HELP_TEXT)
    Response.Write  "</span>"
    Response.Write "<span id=""pickup_date_container"" class=""rightcol"">"

        If EXPECTED_PICKUP_LEADTIME_SOURCE = 0 And CBool(GLOBAL_USE_EXPECTED_SHIP_DATE) Then
            '- Read from SQL Query
            vCart.MoveFirst

            iMaxLeadTime = 0
            '- get MAX lead time from all products in cart...
            Do While Not vCart.EOF
                If CInt(vCart("calculated_lead_time")) > iMaxLeadTime Then
                    iMaxLeadTime = CInt(vCart("calculated_lead_time"))
                End If'-If iMaxLeadTime < vCart("calculated_lead_time") Then
                vCart.MoveNext
            Loop

        Else
            ' - Read from XML option
            iMaxLeadTime = CInt(EXPECTED_PICKUP_FIRST_ELIGIBLE_DATE_LEADTIME)

        End If'- If EXPECTED_PICKUP_LEADTIME_SOURCE = 0 And CBool(GLOBAL_USE_EXPECTED_SHIP_DATE) Then

        'Get the Start date..
        oBusinessDayCalculator.setStartDate

        '- Get Effective order date
        sFirstDate = oBusinessDayCalculator.StartDate & " " & PICKUP_DATE_SUFFFIX_FIRSTDATE

        if DateDiff("d", oBusinessDayCalculator.StartDate, FormatDateTime(Now,2)) = 0 then
            sFirstPulldownItemText = "Today"
        else
            sFirstPulldownItemText = sFirstDate
        end if 'oBusinessDayCalculator.StartDate = FormatDateTime(Now,2) then'

        %>
        <select name="expected_pickup_date"
                id="expected_pickup_date"
                class="pickup-date tooltip-trigger"
                data-title="<%=PICKUP_DATE_TITLE%>"
                data-placement="top"
                >
        <option value="<%=sFirstDate%>"><%=sFirstPulldownItemText%></option><%
        '- Loop out available dates that should start with the effective order date
        '- As first date is define outside the loop. so that i loop is offet by 2.
        'For E.g.
        'if iMaxLeadTime = 2 and EXPECTED_PICKUP_QTY_OF_DAYS_ELIGIBLE = 10, it would loop from 2 to 12 (inclusive) resulting in 11 dates... since you write out the first date seperately, you need to offset the loop by 2 (looping out 9 dates) + the first.

        For iLeadTime=iMaxLeadTime To (iMaxLeadTime+CInt(EXPECTED_PICKUP_QTY_OF_DAYS_ELIGIBLE)-2)
                sOtherDates = oBusinessDayCalculator.ExpectedShipDate(iLeadTime) & " "
                
                If Weekday(sOtherDates) = 7 Then
                    Response.Write "<option value=""" & sOtherDates & "8 AM to 11 AM" & """>" & sOtherDates & "8 AM to 11 AM" &"</option>"
                Else
                    Response.Write "<option value=""" & sOtherDates & PICKUP_DATE_SUFFIX_OTHERDATES & """>" & sOtherDates & PICKUP_DATE_SUFFIX_OTHERDATES &"</option>"
                End If
        Next

        Response.Write "</select>"
        Response.Write "</span></div>"
        %>
        <div id="pickup-date-text" style="display: block;"><strong>Orders to be picked up at a Store Location</strong>
            <ul><hr>
                <li>Available for pick up 2 hours after the order is placed.</li>
                <li>Subject to store stock availability. Not all items are available at all locations.</li>
                <li>Orders placed after 3:00pm (EST) will be ready for pickup after 10:00am the next business day, Monday - Friday.</li>
                <li>Saturday pickup may be available in some cases. Please call ahead to check for availability.</li>
                <li>Credit card and ID required for in-store pickup.</li>
            </ul>
            <p><strong>Store Hours:</strong> M-F 8-5:00pm (EST), Saturday 8-12pm (EST). Closed Sunday.</p>
            <hr>
        </div> <!-- #pickup-date-text --><%

        If ImDebugging() Then
            Response.Write "<div style='border:1px solid #346AAD;font-weight:bold;text-align:left;padding:5px;background-color:#94CCD6;color:#000;font-size:11px;'>Lead Time: " & (iMaxLeadTime) & "</div>"
            Response.Write "<div style='border:1px solid #346AAD;font-weight:bold;text-align:left;padding:5px;background-color:#94CCD6;color:#000;font-size:11px;'>EXPECTED_PICKUP_LEADTIME_SOURCE(0 = sql, 1 = xml): " & EXPECTED_PICKUP_LEADTIME_SOURCE & "</div>"
            Response.Write "<div style='border:1px solid #346AAD;font-weight:bold;text-align:left;padding:5px;background-color:#94CCD6;color:#000;font-size:11px;'>EXPECTED_PICKUP_QTY_OF_DAYS_ELIGIBLE: " & EXPECTED_PICKUP_QTY_OF_DAYS_ELIGIBLE & "</div>"
        End If
End Sub

'****************************************************************************
' fncCustomOrderPostLogic
'
' Custom Post Logic to set expected_order_date on order header. Called via
' hook BillShipActionBillingInfoPostLogic.
' [dcohen @ 2016-11-22] #103359
'****************************************************************************
Function fncCustomOrderPostLogic(ByRef aArgs)
    Dim oPlBillingInfo : Set oPlBillingInfo = aArgs(1)
    Dim aBillingInfoFields : Set aBillingInfoFields = aArgs(2)

    If Trim(oPlBillingInfo.GetPostedValue("expected_pickup_date") & "") <> "" Then
        Call aBillingInfoFields.Add("expected_pickup_date")
    End If

    Call aBillingInfoFields.Add("opt1")
    Call aBillingInfoFields.Add("opt2")

    fncCustomOrderPostLogic = True
End Function


'****************************************************************************
' fncDrawCustomBillShipFormOpenTag
'
' adds accept-charset="UTF-8" to the open form tag to match old custom CFV
' [dcohen @ 2016-11-14] #103359
'****************************************************************************
Function fncDrawCustomBillShipFormOpenTag(ByRef aArgs)
    'Call subDrawBillShipSidebar()
    %>
    <form action="<%=encodeAmpersands(BuildQueryString("sc_id|c_key|a_key|display|type|cat_id|pc_key|path|pcs_key|username|form", "sc_id=" & uSession.SC_ID & "&c_key=" & sCustID & "&a_key=" & sAcntID & "&origin=bill-ship"))%>" method="post" name="form_builder" id="form_builder" class="form-horizontal" accept-charset="UTF-8">
    <%
    fncDrawCustomBillShipFormOpenTag = True
End Function


'****************************************************************************
' subDrawBillShipSidebar - title markup.
' called by fncDrawCustomBillShipFormOpenTag
'
' [dcohen @ 2016-11-22] #103359
'****************************************************************************
Sub subDrawBillShipSidebar()
    Dim oRsCheckoutSidebar
    SIDEBAR_CONTENT_WEBPAGE_KEY="8BF0665113114F45AE56B26FBC53F269"
    ' copied from Bill-Ship - EQUIPPERS
     If Not Request.QueryString("modal") = "1" Then  %>
        <div class="checkout-sidebar">

            <h4>Need Help?</h4>
            <p>Chat 8:00am - 6:00pm EST <br>
            Monday - Friday</p><%
            If bStaticChatIcon or True Then %>
                <!-- BEGIN ProvideSupport.com Custom Images Chat Button Code -->
                <div id="ciAc13" style="z-index:100;position:absolute"></div><div id="scAc13" style="display:inline"></div><div id="sdAc13" style="display:none"></div><script type="text/javascript">var seAc13=document.createElement("script");seAc13.type="text/javascript";var seAc13s=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/1745h6po97tz20810uhsfrm6je/safe-standard.js?ps_h=Ac13&ps_t="+new Date().getTime()+"&online-image=https%3A//www.equippers.com/images/chat-icon-35-online-en.gif&offline-image=https%3A//www.equippers.com/images/chat-icon-35-offline-en.gif";setTimeout("seAc13.src=seAc13s;document.getElementById('sdAc13').appendChild(seAc13)",1)</script><noscript><div style="display:inline"><a href="http://www.providesupport.com?messenger=1745h6po97tz20810uhsfrm6je">Customer Service Chat</a>
                </div></noscript>
                <!-- END ProvideSupport.com Custom Images Chat Button Code -->
            <% Else %>
                    <!-- BEGIN ProvideSupport.com Custom Images Chat Button Code -->
                    <div id="cigDFi" style="z-index:100;position:absolute"></div><div id="scgDFi" style="display:inline;position:fixed;z-index:9999;top:40%;left:0%;"></div><div id="sdgDFi" style="display:none"></div><script type="text/javascript">var segDFi=document.createElement("script");segDFi.type="text/javascript";var segDFis=(location.protocol.indexOf("https")==0?"https":"http")+"://image.providesupport.com/js/1745h6po97tz20810uhsfrm6je/safe-standard.js?ps_h=gDFi&ps_t="+new Date().getTime()+"&online-image=https%3A//www.equippers.com/images/chat-icon-59-online-en.gif&offline-image=https%3A//www.equippers.com/images/chat-icon-59-offline-en.gif";setTimeout("segDFi.src=segDFis;document.getElementById('sdgDFi').appendChild(segDFi)",1)</script><noscript><div style="display:inline;position:fixed;z-index:9999;top:40%;left:0%;"><a href="http://www.providesupport.com?messenger=1745h6po97tz20810uhsfrm6je">Customer Service Chat</a></div></noscript>
                    <!-- END ProvideSupport.com Custom Images Chat Button Code -->
            <% End If

            Set oRsCheckoutSidebar                           = New cDataAccessRecordSet3
            oRsCheckoutSidebar.SQL_ID                        = "Web_Page_Detail"
            oRsCheckoutSidebar.SQL_ID_Minor_VersionNumber    = 0
            oRsCheckoutSidebar.SQL_ID_Revision_VersionNumber = 4
            oRsCheckoutSidebar.ExtraFields                   = "p.ds"
            oRsCheckoutSidebar.SearchString                  = "searchexact~p.p_key~" & SIDEBAR_CONTENT_WEBPAGE_KEY
            oRsCheckoutSidebar.UseDataSets                   = False
            oRsCheckoutSidebar.MaxRecords                    = 1

            Call oRsCheckoutSidebar.Open()

                If Not oRsCheckoutSidebar.EOF Then
                    Response.Write oRsCheckoutSidebar("ds")
                End If

            oRsCheckoutSidebar.Close
            Set oRsCheckoutSidebar = Nothing %>

            <div class="text-center">
                <a href="https://www.securitymetrics.com/site_certificate.adp?s=199.15.174.21&i=426127" target="_blank"><img src="https://www.securitymetrics.com/images/sm_ccsafe_check1.gif" alt="SecurityMetrics for PCI Compliance, QSA, IDS, Penetration Testing, Forensics, and Vulnerability Assessment" border="0" style="width: 100px;"></a>
                <hr>
                <script type="text/javascript" src="https://seal.networksolutions.com/siteseal/javascript/siteseal.js"></script>
                <script language="JavaScript" type="text/javascript"> SiteSeal("https://seal.networksolutions.com/images/basicsqblue.gif", "NETSP", "none");</script>
            </div>

        </div>
        <!-- .checkout-sidebar -->
        <%
    End If
End Sub


'****************************************************************************
' fncDrawCustomAccountInputs
'
' For hook BillShipReplaceActionDrawAccount. Adds custom logic to handle
' address changes
' [dcohen @ 2016-11-14] #103359
'****************************************************************************
Function fncDrawCustomAccountInputs(ByRef aArgs)
    Dim rsBillShipInfo : Set rsBillShipInfo = aArgs(1)
    Dim sAcntID : sAcntID = aArgs(2)

    Dim bTaxExempt, _
        sCompany, _
        sAddress, _
        sAddress2, _
        sCountry

    If Not rsBillShipInfo.EOF Then
        bTaxExempt  = Trim(rsBillShipInfo("tax_exempt") & "")
        sCompany    = Trim(rsBillShipInfo("nm") & "")
        sAddress    = Trim(rsBillShipInfo("address") & "")
        sAddress2   = Trim(rsBillShipInfo("city") & "") & ", " & Trim(rsBillShipInfo("state") & "") & " " & Trim(rsBillShipInfo("zip") & "")
        sCountry    = Trim(rsBillShipInfo("country") & "")

        %><script type="text/javascript">
            window.oBillShipInfo = <%=rsBillShipInfo.BuildJSON()%>[0];
        </script><%
        Call rsBillShipInfo.MoveFirst()

    End If
    If CBool(bTaxExempt) Then
        %><div id="account-address-container"> <%
    Else %>
        <div id="account-address-container" class="hide"> <%
    End If %>
        <input type="hidden" name="tax_status" id="tax_status" value="0" />
        <div id="selector_account" class="module">
            <div class="module-header">
                <a onClick="editAccountInfo()" class="btn btn-mini pull-right">
                    Change
                </a>
                <h4>Account Address</h4>
            </div>
            <div class="module-body">
                <div class="finder">
                    <div id="finder_account" class="finder-selected"> <%
                        If CBool(bTaxExempt) Then %>
                            <div class="formrow">
                                <span class="leftcol"><b class="label label-important">NOTE</b>&nbsp;</span>
                                <span class="rightcol text-error account-note" >Changing the primary account address will remove the tax exemption associated with this address. Please submit your order and contact us with questions or changes.</span>
                            </div> <%
                        End If %>
                        <ul id="customer_information" class="unstyled">
                            <li><%=sCompany%></li>
                            <li><%=sAddress%></li>
                            <li><%=sAddress2%></li>
                            <li><%=sCountry%></li>
                        </ul>
                    </div>
                    <!-- .finder-selected -->
                </div>
                <!-- .finder -->
            </div>
            <!-- .module-body -->
        </div>
        <!-- .module -->
    </div>  <!-- #account-address-container -->
    <%
    If CBool(bTaxExempt) Then
        %><div id="account-edit-container" class="formblock-content hide"><%
    Else %>
        <div id="account-edit-container" class="formblock-content"><%
    End If
    Call drawAccount(sAcntID, rsBillShipInfo)
    %></div><%
    fncDrawCustomAccountInputs = True
End Function

'****************************************************************************
' fncUpdateBillShipExtraFields
'
' This adds tax_exempt to rsBillShipInfo so it can be used above in
' fncDrawCustomAccountInputs
' [dcohen @ 2016-11-15] #103359
'****************************************************************************
Function fncUpdateBillShipExtraFields(ByRef aArgs, ByVal vVal)
    fncUpdateBillShipExtraFields = vVal _
        & ", CASE WHEN (SELECT count(0) FROM customers_tx_codes_map (NOLOCK) WHERE ws_id=a.ws_id AND a_id=a.a_key AND exempt='1') > 0 THEN '1' ELSE '0' END AS tax_exempt" _
        & ", (SELECT dbo.GROUP_CONCAT([state]) FROM customers_tx_codes_map (NOLOCK) WHERE ws_id=c.ws_id AND a_id=a.a_key AND exempt='1') AS exempt_states"
End Function

'****************************************************************************
' fncUpdateAccountTaxMappings
'
' Ported from custom equippers bill-ship page. If the user updates the
' account billing address, update [customers_tx_codes_map] to ensure the
' account is no longer tax-exempt in any location. Equippers will review the
' change and update the tax exemption data in their ERP, which whill then
' be synced back to the website.
' [dcohen @ 2016-11-15] #103359
'****************************************************************************
Function fncUpdateAccountTaxMappings(ByRef aArgs)
    If Trim(Request.Form("tax_status") & "") = "1" then
        Dim rsExec
        Set rsExec = new cDataAccessRecordSet3
        rsExec.execute "UPDATE customers_tx_codes_map SET exempt='0' WHERE ws_id=" & SQLQ(WS_ID) & " AND a_id=" & SQLQ(uSession.AccountKey) & ";"
        set rsExec = Nothing
    End If
    fncUpdateAccountTaxMappings = True
End Function

Function fncDrawCustomBillShipJS(ByRef aArgs)
    %>
    <!-- #include file="BillShipCustomLogic.js" -->
    <%
End Function

'****************************************************************************
' fncGenAccountNumber
'
' Sets the account number for new accounts. Uses functions in CFV include
' LIB Equippers Functions. Set by hook BillShipFilterAccountNumber.
' [dcohen @ 2016-11-16] #103359
'****************************************************************************
Function fncGenAccountNumber(ByRef aArgs, ByVal val)
    Dim oPlAccount : Set oPlAccount = aArgs(2)
    Dim sNewAccountKey : sNewAccountKey = aArgs(3)
    Dim sDivisionCode : sDivisionCode = aArgs(4)

    Dim sAccountPrefix, _
        sResult

    sAccountPrefix = getAccRefIDPrefix(Trim(Request.Form("nm") & ""), Trim(Request.Form("c_l_nm") & ""))

    ' getAccRefIDPrefix and generateAccRefID are both in LIB Equippers Functions.
    'sAccountPrefix = getAccRefIDPrefix(oPlAccount.GetPostedValue("nm"), oPlAccount.GetPostedValue("c_l_nm"))

    sResult = generateAccRefID(sNewAccountKey, sAccountPrefix) ' This actually updates the row in the db.

    fncGenAccountNumber = sResult
End Function

'****************************************************************************
' fncGenAccountRefID
' [dcohen @ 2016-11-16] #103359
'****************************************************************************
Function fncGenAccountRefID(ByRef aArgs, ByVal val)
    Dim sAccountNumber : sAccountNumber = aArgs(5)
    fncGenAccountRefID = sAccountNumber
End Function

'****************************************************************************
' fncCustomAddressPostLogic
'
' custom post logic to set sha.nm to the value in sha.id
' [dcohen @ 2016-11-16] #103359
'****************************************************************************
Function fncCustomAddressPostLogic(ByRef aArgs)
    Dim oPlShippingAddress : Set oPlShippingAddress = aArgs(1)


    oPlShippingAddress.UpdateFormPostValue("nm") = sShipToCode ' oPlShippingAddress.GetPostedValue("sacct_code")

    fncCustomAddressPostLogic = True
End Function

'****************************************************************************
' fncReturnTrue
'
' This is here to prevent the bill-ship page from redirecting back to the
' showcart page because of an "invalid password" error. This needs to be here
' because the default logic expects passwords for new users to be posted from
' the showcart page, but Equippers has a customization in which the new user
' password fields are removed from the showcart page and instead added to the
' bill-ship page.
'
' This same function is used to turn off the "pw" hidden input, which is
' normally passed through from the showcart page, for the same reason.
' [dcohen @ 2016-11-21] #103359
'****************************************************************************
Function fncReturnTrue(ByRef aArgs)
    fncReturnTrue = True
End Function

'****************************************************************************
' fncSetDefaultEmailAddress
' set default email address to the username passed in.
' [dcohen @ 2016-11-21] #103359
'****************************************************************************
Function fncSetDefaultEmailAddress(ByRef aArgs, ByVal val)
    fncSetDefaultEmailAddress = aArgs(2)
End Function

'****************************************************************************
' fncDrawBillShipPasswordInputs
'
' Draw the password inputs at the bottom of the bill-ship page.
' [dcohen @ 2016-11-21] #103359
'****************************************************************************
Function fncDrawBillShipPasswordInputs(ByRef aArgs)
    If bNewCust Or request.querystring("pw") = "invalid" Then
        '*
         ' Invalid Password
         '*
        If request.querystring("pw") = "invalid" Then
            %><span class="mgrfront-error"><%=GLOBAL_PASSWORD_NOT_VALID_MESSAGE%></span><%
        End If '- If Request.QueryString("password") = "invalid" Then %>

        <fieldset id="createPasswordSection" class="formblock">
            <legend>Create Password <small>Check the status of your order, track shipments, and checkout faster next time with a password</small></legend>
            <div class="formblock-content">
                <div class="formrow">
                    <span class="leftcol">Password</span>
                    <span class="rightcol"><input type="password" data-title="Password must be a minimum of 6 characters and can only contain letters, numbers, and the following symbols: dash, period, @, and underscore." data-placement="top" class="forminput tooltip-trigger" name="pw" id="pw"></span>
                </div>
                <div class="formrow">
                    <span class="leftcol">Confirm Password</span>
                    <span class="rightcol"><input type="password" class="forminput" name="pw2" id="pw2"></span>
                </div>
            </div>
        </fieldset> <%
    End If '-If bNewCust Or request.querystring("pw") = "invalid" Then
    fncDrawBillShipPasswordInputs = True
End Function

%>
