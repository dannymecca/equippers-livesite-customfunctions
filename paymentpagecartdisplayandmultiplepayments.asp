<%
Function fncAddTslPayMethodField(aArgs, val)
	fncAddTslPayMethodField = val & ", tsl.cmethod, , svc.base_rate - svc.total as less_free_shipping"
End Function

Function fncPaymentPageReplaceFormActions(aArgs)
	'DO nothing intentionally
	fncPaymentPageReplaceFormActions = True
End Function

Function fncLibCartBuilderActionEndCartGroup(aArgs)
	'- 0: Function Name
	'- 1: Cart Recordset
	'- 2: Last Group Name
	'- 3: Last Group Total
	Dim oCart: Set oCart = aArgs(1)
	Dim sLastGroup: sLastGroup = aArgs(2)	
	Dim dLastGroupTotal

	Dim bStorePickup

	if Trim(sLastGroup) <> ""	Then
		oCart.MovePrevious ' to move back to the last group
		dLastGroupTotal = aArgs(3)
		bStorePickup = CBool(oCart("globaladdress"))
		%>
		</table>
		<% If Trim(sGetPageName) <> "showcart.asp" Then %>
			<div class="cart-group-total">
			<%

				Call drawShippingInfoTable(bStorePickup, oCart)
				Call drawShippingTotalsTable(bStorePickup, oCart, dLastGroupTotal)
			
			%>
			</div><!-- .cart-group-total -->
		<% End If
		oCart.MoveNext ' to move back to where we were
	End if

End Function

Sub drawShippingTotalsTable(bStorePickup, oCart, dLastGroupTotal)
	Dim sShippingTotalsTableHeader : sShippingTotalsTableHeader = fncGetShippingTotalsTableHeader(bStorePickup, oCart)
	Dim sShippingTotalVal : sShippingTotalVal = fncGetShippingTotalsShippingTotalVal(bStorePickup, oCart)
	Dim sTotalClass : sTotalClass = fncGetShippingTotalsTableTotalClass(bStorePickup, oCart)
	Dim sOrderShipmentTotalVal : sOrderShipmentTotalVal = fncGetOrderShipmentTotalVal(bStorePickup, oCart)
	%>
	<table class="cart-group-shipping-totals cart-group-table">
		<thead>
			<tr><th colspan=2><%=sShippingTotalsTableHeader%></th></tr>
		</thead>
		<tbody>
			<tr>
				<td>Item Total</td>
				<td><%=uSession.FormatPrice(dLastGroupTotal, GLOBAL_CART_DECIMAL_PLACES)%></td>
			</tr>
			<tr>
				<td>Shipping Total</td>
				<td><%=sShippingTotalVal%></td>
			</tr>
			<%
			call drawLessFreeShippingRow(oCart)
			%>
			<tr>
				<td>Tax Total</td>
				<td><%=uSession.FormatPrice(oCart("s_t_total"), 2)%></td>
			</tr>
			<tr class="<%=sTotalClass%>">
				<td>Total</td>
				<td><%=sOrderShipmentTotalVal%></td>
			</tr>
			<%
			Call drawShippingTotalsTablePaymentStatus(bStorePickup, oCart, sTotalClass)
			%>
		</tbody>
	</table>
	<script>
		var free_ship = false;
		ofConfig.oOrderJson[1].shipments.filter(function (shipments) {
			if (shipments.selectedShipVia.parent_nm = "ALS"){
				shipments.details.forEach(function(detail){
				if (detail.orderDetail.commodity.freeShipping){
					free_ship=true;
					return true;
					}	
				});
			}
		});
	</script>
	<%
End Sub

Function fncGetShippingTotalsTableHeader(bStorePickup, oCart)
	If Trim(getpagename()) = "confirm.asp" Then
		fncGetShippingTotalsTableHeader = "Order Information"
	Else
		fncGetShippingTotalsTableHeader = oCart("pw_opt5")
	End If
End Function

Function fncGetShippingTotalsShippingTotalVal(bStorePickup, oCart)
	If Not bStorePickup And (instr(oCart("ship_via_nm"),"Customer Service") or Trim(oCart("ship_via_nm")&"") = "") Then
		fncGetShippingTotalsShippingTotalVal = "TBD"
	Else
		If oCart("less_free_shipping") > 0 Then
			fncGetShippingTotalsShippingTotalVal = uSession.FormatPrice((oCart("s_s_total") + oCart("less_free_shipping")), 2)
		Else 
			fncGetShippingTotalsShippingTotalVal = uSession.FormatPrice(oCart("s_s_total"), 2)
		End If
	End If
End Function

Function fncGetShippingTotalsTableTotalClass(bStorePickup, oCart)
	If Trim(getpagename()) = "confirm.asp" Then 
		If lcase(oCart("cmethod")) = "billme" Then
			fncGetShippingTotalsTableTotalClass = "pickup-total"
		Else
			fncGetShippingTotalsTableTotalClass = "total"
		End If
	Else
		fncGetShippingTotalsTableTotalClass = "total"
	End If
End Function

Function fncGetOrderShipmentTotalVal(bStorePickup, oCart)
	If Not bStorePickup And (instr(oCart("ship_via_nm"),"Customer Service") or Trim(oCart("ship_via_nm")&"") = "") Then
		fncGetOrderShipmentTotalVal = "TBD"
	Else
		fncGetOrderShipmentTotalVal = uSession.FormatPrice(oCart("shipment_total"), 2)
	End If
End Function

Sub drawLessFreeShippingRow(oCart)
	If oCart("pw_key") = "AB5605934DCA4BBAA54C377CC04B624B" and oCart("less_free_shipping") > 0 Then %>
		<tr>
			<td><strong>Less Free Shipping</strong></td>
			<td>(<%=uSession.FormatPrice(oCart("less_free_shipping"),2)%>)</td>
		</tr>
	<% End If
End Sub

Sub drawShippingTotalsTablePaymentStatus(bStorePickup, oCart, sTotalClass)
	If Trim(getpagename()) <> "confirm.asp" Then
		' Only draw the payment status row on the order confirmation page
		Exit Sub
	End If

	Dim sTableRowClass : sTableRowClass = "payment-status " & sTotalClass

	%>
	<tr class="<%=sTableRowClass%>">
		<td>Payment Status</td>
		<%
		Call drawPaymentStatusVal(bStorePickup, oCart)
		%>
	</tr>
	<%
End Sub

Sub drawPaymentStatusVal(bStorePickup, oCart)
	If bStorePickup Then
		%>
		<td class="offline-total">Pay in Store</td>
		<%
	ElseIf lcase(oCart("cmethod")) = "billme" Or instr(oCart("ship_via_nm"),"Customer Service") or Trim(oCart("ship_via_nm")&"") = "" Then
		%>
		<td>Call to Finalize 800-235-3325</td>
		<%
	Else
		%>
		<td>Payment authorized</td>
		<%
	End If
End Sub

Sub drawShippingInfoTable(bStorePickup, oCart)
	Dim sShippingInfoTableHeader
	sShippingInfoTableHeader = fncGetShippingInfoTableHeader(bStorePickup, oCart)
	%>
	<table class="cart-group-shipping-info cart-group-table" cellspacing="0">
		<thead>
			<tr>
				<th colspan=2><%=sShippingInfoTableHeader%></th>
			</tr>
		</thead>
		<tbody>
			<%
			Call drawShippingInfoTableBody(bStorePickup, oCart)
			%>
		</tbody>
	</table>
	<%
End Sub

Function fncGetShippingInfoTableHeader(bStorePickup, oCart)
	If bStorePickup Then
		fncGetShippingInfoTableHeader = "Pick Up Information"
	Else
		fncGetShippingInfoTableHeader = "Shipping Information"
	End If
End Function

Sub drawShippingInfoTableBody(bStorePickup, oCart)
	If bStorePickup Then
		Call drawShippingInfoTableBodyStorePickup(oCart)
	Else
		Call drawShippingInfoTableBodyStandard(oCart)
	End If
End Sub

Sub drawShippingInfoTableBodyStorePickup(oCart)
	%>
	<tr>
		<td>
			<div class="cart-group-pickup-info">
				<span class="pickup-info-warehouse"><%=oCart("pw_nm")%></span><br>
				<%=oCart("pw_add1")%><br/>
				<%=oCart("pw_city")%>, <%=oCart("pw_state")%>&nbsp;<%=oCart("pw_zip")%><br/>
				<%=oCart("pw_ds")%>
			</div>
		</td>
	</tr>
	<%
End Sub

Sub drawShippingInfoTableBodyStandard(oCart)
	Dim sShippingCarrier : sShippingCarrier = fncGetShippingCarrier(oCart)
	%>
	<tr>
		<td>Shipping Carrier</td>
		<td><%=sShippingCarrier%></td>
	</tr>
	<tr>
		<td>Estimated Ship Date</td>
		<td><%=oCart("pw_opt4")%></td>
	</tr><%
	If oCart("drop_ship_type")&"" = "only" Then %>
		<tr>
			<td>Delivery Call Ahead Information</td>
			<td>
				<%=oCart("order_attention")%><br>
				<%=oCart("order_phone")%>
			</td>
		</tr><%
	ElseIf false Then 'Hiding this per task #119689 - Can be deleted once confirmed. %>
		<tr>
			<td>Shipping Contact</td>
			<td>
				<%=uSession.CustomerFirstName & " " & uSession.CustomerLastName%><br>
				<%=uSession.CustomerField("c_phone")%>
			</td>
		</tr><%
	End If %>
	<%
End Sub

Function fncGetShippingCarrier(oCart)
	If (oCart("pw_add1") = "99") Then
		fncGetShippingCarrier = oCart("ship_via_nm")
	Else
		fncGetShippingCarrier = oCart("pw_opt3")
	End If
End Function

Function fncPaymentAjaxGetShipViasDroplist(aArgs)
	'- 0: Func Name
	'- 1: Ship Via Recordset
	Response.Clear
	Response.Write "HERE - HERE - HERE"
	Response.End
End Function
%> 