<%

'****************************************************************************
' fncDrawCustomCheckoutForm
'
' Replicate existing checkout form logic from custom showcart page
'
' [dcohen @ 2016-11-21] 103359
'****************************************************************************
Function fncDrawCustomCheckoutForm(ByRef aArgs)
    If aArgs(2) = "custom-newcust" Then
        %>
<div class="span4 offset1">
    <div class="well well-large well-highlight">
        <h4 class="text-info">I'm New Here</h4>
        <p class="muted"><small>Let's start with an email address:</small></p>

        <div class="alert alert-error" style="<%=sNewError%>">
            <%=NERROR_MSG_TEXT%>
        </div>

        <form   class="form-checkout clearfix"
                id="frmNewCust"
                name="newuserform"
                method="POST"
                action="<%=ORDERFRONTURL%>/<%=CREATE_ACCOUNT_PROCEED_TO_CHECKOUT_PAGE%>?form=checklogon&amp;<%=uSession.BuildSessionPassThrough%>"
                autocapitalize="off"
                autocorrect="off"
                autocomplete="off"
            >
            <div class="control-group">
                <input type="email" name="nusername" id="nusername">
                <button type="submit" class="btn-main pull-left continue">Continue</button>
            </div>
        </form>

        <% ' previously TEXT_BELOW_NEW_USER %>
        <ul class="icons-ul">
            <li> <i class="icon-li icon-large icon-ok-sign text-success"></i>
                Open to the Public
            </li>
            <li> <i class="icon-li icon-large icon-ok-sign text-success"></i>
                Shipping fee <b>includes liftgate</b>
                and call-ahead (if applicable)
            </li>
            <li>
                <i class="icon-li icon-large icon-ok-sign text-success"></i>
                Buy confidently with our comprehensive
                <a href="http://www.equippers.com/public_html/returnpolicy.asp">return policy</a>
            </li>
        </ul>
    </div>
</div>
<div class="span4 offset1">
    <div class="well well-large">
        <h4 class="text-info">Sign in to My Account</h4>
        <p class="muted">
            <small>Enter your email address and password below:</small>
        </p><% 
        sLoginFormLink = ORDERFRONTURL & "/security_logonscript_siteFront.asp?pageredir=" & Server.UrlEncode(getNextPageUrl(RETURN_CUSTOMER_PROCEED_TO_CHECKOUT_PAGE & "?custtype=retcust&" & uSession.BuildSessionPassthrough)) & "&returnpage=showcart.asp"
        %>
        <FORM name="retuserform" method="POST" action="<%=sLoginFormLink%>" onsubmit="return checkreturn(this);" class="clearfix">
            <label for="email">
                Email Address
                <small>(required)</small>
            </label>
            <input type="email" name="username" id="username">
            <label for="password">
                Password
                <small>(required)</small>
            </label>
            <input type="password" name="password" id="password">
            <button type="submit" class="btn-main btn-main-alt">Sign In</button>
        </form>
        <p>
            <a href="<%=ORDERFRONTURL%>/request_password.asp">Forgot your login?</a>
        </p>
    </div>
</div>
<%
    End If ' If aArgs(2) = "custom-newcust" Then
    fncDrawCustomCheckoutForm = True
End Function

%>
