<%
'**
 ' 
'**
Public Function fncShowCartActionTotalsSection(aArgs) 
	
	' DM:  11.1.17 
	' Removed ship_zip from show cart xml option and added below. %>

	<div id="ship_zip">
	   <label for="zipcode">Calculate Shipping:</label><br />
	   <input type="text" name="zipcode" id="zipcode" width="40%" value="Enter your zip code"><input class="formBtn" type="button" name="ship_go" id="ship_go" value="Go">
	   
	   <div id="td_shipmethods"></div>
	</div>
	<div id="cart_totals">
		<table class="table">
			<tr>
				<td><strong>Product Total:</strong></td>
				<td align="right"><strong><span id="td_carttotal"><%=uSession.FormatMoney(Round(aArgs(1).ExtTotal,2))%></span></strong>
					<input type="hidden" name="OTotal" id="OTotal" value="<%=Round(aArgs(1).ExtTotal,2)%>"/></td>
			</tr>
			<%
			If DiscAmount > 0 Then %>
			<tr>
				<td><strong class="less_shipping red">Discount Amount</strong></td>
				<td align="right"><strong><span id="td_disctotal" class="less_shipping red"><%=uSession.FormatMoney(-DiscAmount)%></span></strong>
					<input type="hidden" name="disctotal" id="disctotal" value="<%=Round(DiscAmount,2)%>"/></td>
			</tr>
			<%
			ElseIf CouponAmount > 0 Then%>
			<tr>
				<td><strong class="less_shipping red">Discount Amount</strong></td>
				<td align="right"><strong><span id="td_disctotal" class="less_shipping red"><%=uSession.FormatMoney(-CouponAmount)%></span></strong>
					<input type="hidden" name="disctotal" id="disctotal" value="<%=Round(CouponAmount,2)%>"/></td>
			</tr>
			<%
			End if
			%>
			<tr>
				<td><strong><span id="td_shiptotal_label">Proceed for Shipping</span></strong></td>
				<td align="right"><strong><span id="td_shiptotal"></span></strong>
					<INPUT TYPE="hidden" name="shiptotal" id="shiptotal" value=""/>
					<INPUT TYPE="hidden" name="shipdiscount" id="shipdiscount" value=""/>
				</td>
			</tr>
			<tr class="success">
				<td class="text-success"><strong>Grand Total:</strong></td>
				<td class="text-success" valign="top" align="right"><strong><span id="td_grandtotal"><%=uSession.FormatMoney(aArgs(1).ExtTotal)%></span></strong></td>
			</tr><%
			If Trim(FREIGHT_MSG & "") <> "" Then %>
				<tr id="order_totals_freight_msg">
					<td class="freight_msg" colspan="2" style="display:none;">*<%=FREIGHT_MSG%></td>
				</tr>
			<% End If %>
		</table> 
	</div><%
End Function
%>


